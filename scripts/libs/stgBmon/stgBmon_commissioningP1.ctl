//--------------------------------------------------------------------------------
/*
  @author Stamatios Tzanos, NTUA (stzanos: stamatios.tzanos@cern.ch)

  "stgBmon_commissioning.ctl" is a library for the commissioning of the
  bfield sensors at P1.
*/

#uses "stgBmon/stgBmon_conversions.ctl"

string sys = getSystemName();

void stgBmon_P1_create_upper_addressing_A() {

  //
  // ADDRESSING OF UPPER HALF OF THE WHEEL
  //

  // Values of the fwPeriphAddress
  string serverName = "OPCUACANOPENSERVER";
  int driver = 9;
  string opcKind = "1";
  string opcVariant = "1";
  int dataType = 0; // Default
  int mode = 2; // Input
  string opcSubscription = "bfield_data";
  string pollGroup = "";
  dyn_string exceptionInfo;
  bool setWait = TRUE;

  // OPC Items
  dyn_string opc_ids = makeDynString("ns=2;s=EIL4_A.NSW_S01_STG.NodeId",
                                     "ns=2;s=EIL4_A.NSW_S01_MMG.NodeId",
                                     "ns=2;s=EIL4_A.NSW_S02_STG.NodeId",
                                     "ns=2;s=EIL4_A.NSW_S02_MMG.NodeId",
                                     "ns=2;s=EIL4_A.NSW_S03_STG.NodeId",
                                     "ns=2;s=EIL4_A.NSW_S03_MMG.NodeId",
                                     "ns=2;s=EIL4_A.NSW_S04_STG.NodeId",
                                     "ns=2;s=EIL4_A.NSW_S04_MMG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S05_STG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S05_MMG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S06_STG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S06_MMG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S07_STG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S07_MMG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S08_STG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S08_MMG.NodeId");

  dyn_string opc_adcs = makeDynString("ns=2;s=EIL4_A.NSW_S01_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_A.NSW_S01_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_A.NSW_S02_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_A.NSW_S02_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_A.NSW_S03_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_A.NSW_S03_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_A.NSW_S04_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_A.NSW_S04_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S05_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S05_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S06_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S06_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S07_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S07_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S08_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S08_MMG.TPDO4.bfadc_");

  // DPs
  dyn_string dpe_ids = makeDynString(sys + "EIZ4A01.node3.id",
                                     sys + "EIZ4A01.node4.id",
                                     sys + "EIZ4A03.node1.id",
                                     sys + "EIZ4A03.node2.id",
                                     sys + "EIZ4A03.node3.id",
                                     sys + "EIZ4A03.node4.id",
                                     sys + "EIZ4A05.node1.id",
                                     sys + "EIZ4A05.node2.id",
                                     sys + "EIZ4A05.node3.id",
                                     sys + "EIZ4A05.node4.id",
                                     sys + "EIZ4A07.node1.id",
                                     sys + "EIZ4A07.node2.id",
                                     sys + "EIZ4A07.node3.id",
                                     sys + "EIZ4A07.node4.id",
                                     sys + "EIZ4A09.node1.id",
                                     sys + "EIZ4A09.node2.id");

  dyn_string dpe_adcs = makeDynString(sys + "EIZ4A01.node3.data.adc",
                                     sys + "EIZ4A01.node4.data.adc",
                                     sys + "EIZ4A03.node1.data.adc",
                                     sys + "EIZ4A03.node2.data.adc",
                                     sys + "EIZ4A03.node3.data.adc",
                                     sys + "EIZ4A03.node4.data.adc",
                                     sys + "EIZ4A05.node1.data.adc",
                                     sys + "EIZ4A05.node2.data.adc",
                                     sys + "EIZ4A05.node3.data.adc",
                                     sys + "EIZ4A05.node4.data.adc",
                                     sys + "EIZ4A07.node1.data.adc",
                                     sys + "EIZ4A07.node2.data.adc",
                                     sys + "EIZ4A07.node3.data.adc",
                                     sys + "EIZ4A07.node4.data.adc",
                                     sys + "EIZ4A09.node1.data.adc",
                                     sys + "EIZ4A09.node2.data.adc");


  for(int i = 1; i <= 16; i++) {

    fwPeriphAddress_setOPCUA(dpe_ids[i],
                                 serverName,
                                 driver,
                                 opc_ids[i],
                                 opcSubscription,
                                 opcKind,
                                 opcVariant,
                                 dataType,
                                 mode,
                                 pollGroup,
                                 exceptionInfo,
                                 setWait);

    for(int j = 0; j < 16; j++) {

      fwPeriphAddress_setOPCUA(dpe_adcs[i] + j,
                                 serverName,
                                 driver,
                                 opc_adcs[i] + j,
                                 opcSubscription,
                                 opcKind,
                                 opcVariant,
                                 dataType,
                                 mode,
                                 pollGroup,
                                 exceptionInfo,
                                 setWait);
    }
  }
}

void stgBmon_P1_create_lower_addressing_A() {

  //
  // ADDRESSING OF LOWER HALF OF THE WHEEL
  //

  // Values of the fwPeriphAddress
  string serverName = "OPCUACANOPENSERVER";
  int driver = 9;
  string opcKind = "1";
  string opcVariant = "1";
  int dataType = 0; // Default
  int mode = 2; // Input
  string opcSubscription = "bfield_data";
  string pollGroup = "";
  dyn_string exceptionInfo;
  bool setWait = TRUE;

  // OPC Items
  dyn_string opc_ids = makeDynString("ns=2;s=EI_A_top.NSW_S09_STG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S09_MMG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S10_STG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S10_MMG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S11_STG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S11_MMG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S12_STG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S12_MMG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S13_STG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S13_MMG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S14_MMG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S14_STG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S15_STG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S15_MMG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S16_MMG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S16_STG.NodeId");

  dyn_string opc_adcs = makeDynString("ns=2;s=EI_A_top.NSW_S09_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S09_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S10_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S10_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S11_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S11_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S12_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S12_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S13_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S13_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S14_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S14_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S15_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S15_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S16_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S16_STG.TPDO4.bfadc_");

  // DPs
  dyn_string dpe_ids = makeDynString(sys + "EIZ4A09.node3.id",
                                     sys + "EIZ4A09.node4.id",
                                     sys + "EIZ4A11.node1.id",
                                     sys + "EIZ4A11.node2.id",
                                     sys + "EIZ4A11.node3.id",
                                     sys + "EIZ4A11.node4.id",
                                     sys + "EIZ4A13.node1.id",
                                     sys + "EIZ4A13.node2.id",
                                     sys + "EIZ4A13.node3.id",
                                     sys + "EIZ4A13.node4.id",
                                     sys + "EIZ4A15.node1.id",
                                     sys + "EIZ4A15.node2.id",
                                     sys + "EIZ4A15.node3.id",
                                     sys + "EIZ4A15.node4.id",
                                     sys + "EIZ4A01.node1.id",
                                     sys + "EIZ4A01.node2.id");

  dyn_string dpe_adcs = makeDynString(sys + "EIZ4A09.node3.bf.data.adc",
                                     sys + "EIZ4A09.node4.bf.data.adc",
                                     sys + "EIZ4A11.node1.bf.data.adc",
                                     sys + "EIZ4A11.node2.bf.data.adc",
                                     sys + "EIZ4A11.node3.bf.data.adc",
                                     sys + "EIZ4A11.node4.bf.data.adc",
                                     sys + "EIZ4A13.node1.bf.data.adc",
                                     sys + "EIZ4A13.node2.bf.data.adc",
                                     sys + "EIZ4A13.node3.bf.data.adc",
                                     sys + "EIZ4A13.node4.bf.data.adc",
                                     sys + "EIZ4A15.node1.bf.data.adc",
                                     sys + "EIZ4A15.node2.bf.data.adc",
                                     sys + "EIZ4A15.node3.bf.data.adc",
                                     sys + "EIZ4A15.node4.bf.data.adc",
                                     sys + "EIZ4A01.node1.bf.data.adc",
                                     sys + "EIZ4A01.node2.bf.data.adc");


  for(int i = 1; i <= 16; i++) {

    fwPeriphAddress_setOPCUA(dpe_ids[i],
                                 serverName,
                                 driver,
                                 opc_ids[i],
                                 opcSubscription,
                                 opcKind,
                                 opcVariant,
                                 dataType,
                                 mode,
                                 pollGroup,
                                 exceptionInfo,
                                 setWait);

    for(int j = 0; j < 16; j++) {

      fwPeriphAddress_setOPCUA(dpe_adcs[i] + j,
                                 serverName,
                                 driver,
                                 opc_adcs[i] + j,
                                 opcSubscription,
                                 opcKind,
                                 opcVariant,
                                 dataType,
                                 mode,
                                 pollGroup,
                                 exceptionInfo,
                                 setWait);
    }
  }
}

void stgBmon_P1_create_upper_addressing_C() {

 //
  // ADDRESSING OF UPPER HALF OF THE WHEEL
  //

  // Values of the fwPeriphAddress
  string serverName = "OPCUACANOPENSERVER";
  int driver = 9;
  string opcKind = "1";
  string opcVariant = "1";
  int dataType = 0; // Default
  int mode = 2; // Input
  string opcSubscription = "bfield_data";
  string pollGroup = "";
  dyn_string exceptionInfo;
  bool setWait = TRUE;

  // OPC Items
  dyn_string opc_ids = makeDynString("ns=2;s=EI_A_bottom.NSW_S01_STG.NodeId",
                                     "ns=2;s=EI_A_bottom.NSW_S01_MMG.NodeId",
                                     "ns=2;s=EI_A_bottom.NSW_S02_STG.NodeId",
                                     "ns=2;s=EI_A_bottom.NSW_S02_MMG.NodeId",
                                     "ns=2;s=EI_A_bottom.NSW_S03_STG.NodeId",
                                     "ns=2;s=EI_A_bottom.NSW_S03_MMG.NodeId",
                                     "ns=2;s=EI_A_bottom.NSW_S04_STG.NodeId",
                                     "ns=2;s=EI_A_bottom.NSW_S04_MMG.NodeId",
                                     "ns=2;s=EI_A_bottom.NSW_S05_STG.NodeId",
                                     "ns=2;s=EI_A_bottom.NSW_S05_MMG.NodeId",
                                     "ns=2;s=EI_A_bottom.NSW_S06_STG.NodeId",
                                     "ns=2;s=EI_A_bottom.NSW_S06_MMG.NodeId",
                                     "ns=2;s=EI_A_bottom.NSW_S07_STG.NodeId",
                                     "ns=2;s=EI_A_bottom.NSW_S07_MMG.NodeId",
                                     "ns=2;s=EI_A_bottom.NSW_S08_STG.NodeId",
                                     "ns=2;s=EI_A_bottom.NSW_S08_MMG.NodeId");

  dyn_string opc_adcs = makeDynString("ns=2;s=EI_A_bottom.NSW_S01_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_bottom.NSW_S01_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_bottom.NSW_S02_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_bottom.NSW_S02_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_bottom.NSW_S03_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_bottom.NSW_S03_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_bottom.NSW_S04_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_bottom.NSW_S04_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_bottom.NSW_S05_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_bottom.NSW_S05_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_bottom.NSW_S06_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_bottom.NSW_S06_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_bottom.NSW_S07_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_bottom.NSW_S07_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_bottom.NSW_S08_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_bottom.NSW_S08_MMG.TPDO4.bfadc_");

  // DPs
  dyn_string dpe_ids = makeDynString(sys + "EIZ4A01.node3.id",
                                     sys + "EIZ4A01.node4.id",
                                     sys + "EIZ4A03.node1.id",
                                     sys + "EIZ4A03.node2.id",
                                     sys + "EIZ4A03.node3.id",
                                     sys + "EIZ4A03.node4.id",
                                     sys + "EIZ4A05.node1.id",
                                     sys + "EIZ4A05.node2.id",
                                     sys + "EIZ4A05.node3.id",
                                     sys + "EIZ4A05.node4.id",
                                     sys + "EIZ4A07.node1.id",
                                     sys + "EIZ4A07.node2.id",
                                     sys + "EIZ4A07.node3.id",
                                     sys + "EIZ4A07.node4.id",
                                     sys + "EIZ4A09.node1.id",
                                     sys + "EIZ4A09.node2.id");

  dyn_string dpe_adcs = makeDynString(sys + "EIZ4A01.node3.bf.data.adc",
                                     sys + "EIZ4A01.node4.bf.data.adc",
                                     sys + "EIZ4A03.node1.bf.data.adc",
                                     sys + "EIZ4A03.node2.bf.data.adc",
                                     sys + "EIZ4A03.node3.bf.data.adc",
                                     sys + "EIZ4A03.node4.bf.data.adc",
                                     sys + "EIZ4A05.node1.bf.data.adc",
                                     sys + "EIZ4A05.node2.bf.data.adc",
                                     sys + "EIZ4A05.node3.bf.data.adc",
                                     sys + "EIZ4A05.node4.bf.data.adc",
                                     sys + "EIZ4A07.node1.bf.data.adc",
                                     sys + "EIZ4A07.node2.bf.data.adc",
                                     sys + "EIZ4A07.node3.bf.data.adc",
                                     sys + "EIZ4A07.node4.bf.data.adc",
                                     sys + "EIZ4A09.node1.bf.data.adc",
                                     sys + "EIZ4A09.node2.bf.data.adc");


  for(int i = 1; i <= 16; i++) {

    fwPeriphAddress_setOPCUA(dpe_ids[i],
                                 serverName,
                                 driver,
                                 opc_ids[i],
                                 opcSubscription,
                                 opcKind,
                                 opcVariant,
                                 dataType,
                                 mode,
                                 pollGroup,
                                 exceptionInfo,
                                 setWait);

    for(int j = 0; j < 16; j++) {

      fwPeriphAddress_setOPCUA(dpe_adcs[i] + j,
                                 serverName,
                                 driver,
                                 opc_adcs[i] + j,
                                 opcSubscription,
                                 opcKind,
                                 opcVariant,
                                 dataType,
                                 mode,
                                 pollGroup,
                                 exceptionInfo,
                                 setWait);
    }
  }
}

void stgBmon_P1_create_lower_addressing_C() {

  //
  // ADDRESSING OF LOWER HALF OF THE WHEEL
  //

  // Values of the fwPeriphAddress
  string serverName = "OPCUACANOPENSERVER";
  int driver = 9;
  string opcKind = "1";
  string opcVariant = "1";
  int dataType = 0; // Default
  int mode = 2; // Input
  string opcSubscription = "bfield_data";
  string pollGroup = "";
  dyn_string exceptionInfo;
  bool setWait = TRUE;

  // OPC Items
  dyn_string opc_ids = makeDynString("ns=2;s=EI_A_top.NSW_S09_STG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S09_MMG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S10_STG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S10_MMG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S11_STG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S11_MMG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S12_STG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S12_MMG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S13_STG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S13_MMG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S14_MMG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S14_STG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S15_STG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S15_MMG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S16_MMG.NodeId",
                                     "ns=2;s=EI_A_top.NSW_S16_STG.NodeId");

  dyn_string opc_adcs = makeDynString("ns=2;s=EI_A_top.NSW_S09_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S09_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S10_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S10_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S11_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S11_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S12_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S12_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S13_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S13_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S14_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S14_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S15_STG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S15_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S16_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EI_A_top.NSW_S16_STG.TPDO4.bfadc_");

  // DPs
  dyn_string dpe_ids = makeDynString(sys + "EIZ4A09.node3.id",
                                     sys + "EIZ4A09.node4.id",
                                     sys + "EIZ4A11.node1.id",
                                     sys + "EIZ4A11.node2.id",
                                     sys + "EIZ4A11.node3.id",
                                     sys + "EIZ4A11.node4.id",
                                     sys + "EIZ4A13.node1.id",
                                     sys + "EIZ4A13.node2.id",
                                     sys + "EIZ4A13.node3.id",
                                     sys + "EIZ4A13.node4.id",
                                     sys + "EIZ4A15.node1.id",
                                     sys + "EIZ4A15.node2.id",
                                     sys + "EIZ4A15.node3.id",
                                     sys + "EIZ4A15.node4.id",
                                     sys + "EIZ4A01.node1.id",
                                     sys + "EIZ4A01.node2.id");

  dyn_string dpe_adcs = makeDynString(sys + "EIZ4A09.node3.bf.data.adc",
                                     sys + "EIZ4A09.node4.bf.data.adc",
                                     sys + "EIZ4A11.node1.bf.data.adc",
                                     sys + "EIZ4A11.node2.bf.data.adc",
                                     sys + "EIZ4A11.node3.bf.data.adc",
                                     sys + "EIZ4A11.node4.bf.data.adc",
                                     sys + "EIZ4A13.node1.bf.data.adc",
                                     sys + "EIZ4A13.node2.bf.data.adc",
                                     sys + "EIZ4A13.node3.bf.data.adc",
                                     sys + "EIZ4A13.node4.bf.data.adc",
                                     sys + "EIZ4A15.node1.bf.data.adc",
                                     sys + "EIZ4A15.node2.bf.data.adc",
                                     sys + "EIZ4A15.node3.bf.data.adc",
                                     sys + "EIZ4A15.node4.bf.data.adc",
                                     sys + "EIZ4A01.node1.bf.data.adc",
                                     sys + "EIZ4A01.node2.bf.data.adc");


  for(int i = 1; i <= 16; i++) {

    fwPeriphAddress_setOPCUA(dpe_ids[i],
                                 serverName,
                                 driver,
                                 opc_ids[i],
                                 opcSubscription,
                                 opcKind,
                                 opcVariant,
                                 dataType,
                                 mode,
                                 pollGroup,
                                 exceptionInfo,
                                 setWait);

    for(int j = 0; j < 16; j++) {

      fwPeriphAddress_setOPCUA(dpe_adcs[i] + j,
                                 serverName,
                                 driver,
                                 opc_adcs[i] + j,
                                 opcSubscription,
                                 opcKind,
                                 opcVariant,
                                 dataType,
                                 mode,
                                 pollGroup,
                                 exceptionInfo,
                                 setWait);
    }
  }
}

void stgBmon_P1_draw_MDMs(string side, int sector) {

  int mask;

  // Draw tables
  dyn_float x_table = makeDynString(30, 450, 30, 450);
  dyn_float y_table = makeDynString(505, 505, 730, 730);

  // Draw MDMs
  dyn_float x_MDM = makeDynString(100, 520, 100, 520);
  dyn_float y_MDM = makeDynString(455, 455, 680, 680);

  // Draw adc values
  float x_adc_1 = 120;
  float x_adc_2 = 540;
  float y_adc_1 = 535;
  float y_adc_2 = 760;
  float x_adc_off = 70;
  float y_adc_off = 20;
  dyn_float x_adc = makeDynString(x_adc_1, x_adc_2, x_adc_1, x_adc_2);
  dyn_float y_adc = makeDynString(y_adc_1, y_adc_1, y_adc_2, y_adc_2);
  dyn_float x_adc_offset = makeDynString(0, x_adc_off, 2*x_adc_off, 3*x_adc_off);
  dyn_float y_adc_offset = makeDynString(0, y_adc_off, 2*y_adc_off, 3*y_adc_off);

  for(int j = 1; j <= 4; j++) {

    if(!shapeExists("adc_table" + j))
      addSymbol(myModuleName(), myPanelName(),
                "objects/stgBmon/stgBmonCommissioningP1/STG_BFIELD_P1_COM_ADC_TABLE.xml",
                "adc_table" + j, "",
                x_table[j], y_table[j], 0,
                0.65, 0.65);

    mask = stgBmon_nodeToMaskInt(j);
    if(!shapeExists("MDM" + j))
      addSymbol(myModuleName(), myPanelName(),
                "objects/stgBmon/stgBmonCommissioningP1/STG_BFIELD_P1_COM_MDM.xml",
                "MDM" + j, makeDynString("$side:" + side, "$sector:" + sector, "$node:" + j, "$mask:" + mask),
                x_MDM[j], y_MDM[j], 0,
                0.6, 0.6);

    int adc_counter = 0;
    for(int a = 1; a <= 4; a++) {
      for(int b = 1; b <= 4; b++) {
        if(!shapeExists("adc" + (string)j + (string)adc_counter))
          addSymbol(myModuleName(), myPanelName(),
                "objects/stgBmon/stgBmonCommissioningP1/STG_BFIELD_P1_COM_ADC.xml",
                "adc" + (string)j + (string)adc_counter, makeDynString("$side:" + side,
                                                                       "$sector:" + sector,
                                                                       "$node:" + j,
                                                                       "$adc_field:" + adc_counter),
                x_adc[j] + x_adc_offset[b], y_adc[j] + y_adc_offset[a], 0,
                0.6, 0.6);
        adc_counter++;
      }
    }
  }
}

void stgBmon_P1_remove_MDMs() {

  for(int j = 1; j <= 4; j++) {

    if(shapeExists("adc_table" + j))
      removeSymbol(myModuleName(), myPanelName(), "adc_table" + j);
    if(shapeExists("MDM" + j))
      removeSymbol(myModuleName(), myPanelName(), "MDM" + j);

    int adc_counter = 0;
    for(int a = 1; a <= 4; a++) {
      for(int b = 1; b <= 4; b++) {
        if(shapeExists("adc" + (string)j + (string)adc_counter))
          removeSymbol(myModuleName(), myPanelName(), "adc" + (string)j + (string)adc_counter);
        adc_counter++;
      }
    }
  }
}
