//--------------------------------------------------------------------------------
/*
  @author Stamatios Tzanos, NTUA (stzanos: stamatios.tzanos@cern.ch)

  "stgBmon_panel_graphics.ctl" is a library that lists all panel actions that are
  completed to navigate around different panel graphical user interfaces.
*/

// ---------------------------------------------------------------------

void stgBmon_draw_colormap(float x, float y) {

  if(!shapeExists("view_colormap"))
    addSymbol(myModuleName(), myPanelName(),
              "objects/stgBmon/Bmon_view_colormap.xml",
              "view_colormap", "",
              x, y, "",
              1, 1);
}

void stgBmon_draw_mdm_map(float x, float y) {

  if(!shapeExists("view_mdm_map"))
    addSymbol(myModuleName(), myPanelName(),
              "objects/stgBmon/Bmon_view_MDMmap.xml",
              "view_mdm_map", "",
              x, y, "",
              1, 1);
}

void stgBmon_draw_sector(float x, float y, dyn_string params) {

  if(!shapeExists("view_sector"))
    addSymbol(myModuleName(), myPanelName(),
              "objects/stgBmon/Bmon_view_sector.xml",
              "view_sector", params,
              x, y, "",
              1, 1);
}

void stgBmon_draw_mdm(float x, float y, dyn_string params) {

  if(!shapeExists("view_mdm"))
    addSymbol(myModuleName(), myPanelName(),
              "objects/stgBmon/Bmon_view_MDM.xml",
              "view_mdm", params,
              x, y, "",
              1, 1);
}

void stgBmon_draw_sensor(float x, float y, dyn_string params) {

  if(!shapeExists("view_sensor"))
    addSymbol(myModuleName(), myPanelName(),
              "objects/stgBmon/Bmon_view_sensor.xml",
              "view_sensor", params,
              x, y, "",
              1, 1);
}

void stgBmon_remove_colormap() {

  int k;

  if(shapeExists("view_colormap"))
    removeSymbol(myModuleName(), myPanelName(), "view_colormap");
  for(int i = 1; i <= 8; i++) {

    k = 2*i - 1;
    if(shapeExists("Colormap_LDW_A" + k))
      removeSymbol(myModuleName(), myPanelName(), "Colormap_LDW_A" + k);
    if(shapeExists("Colormap_LDW_C" + k))
      removeSymbol(myModuleName(), myPanelName(), "Colormap_LDW_C" + k);
  }

}



void stgBmon_remove_mdm_map() {

  if(shapeExists("view_mdm_map"))
    removeSymbol(myModuleName(), myPanelName(), "view_mdm_map");
}


void stgBmon_remove_sector() {

  if(shapeExists("view_sector"))
    removeSymbol(myModuleName(), myPanelName(), "view_sector");
  if(shapeExists("single_sector"))
    removeSymbol(myModuleName(), myPanelName(), "single_sector");

  for(int i = 1; i <= 12; i++) {

    if(shapeExists("Wedge_sensor_" + i))
      removeSymbol(myModuleName(), myPanelName(), "Wedge_sensor_" + i);
  }

  for(int j = 1; j <= 4; j++) {

    if(shapeExists("MDM_" + j))
      removeSymbol(myModuleName(), myPanelName(), "MDM_" + j);
  }
}

void stgBmon_remove_mdm() {

  if(shapeExists("MDM_plot"))
    removeSymbol(myModuleName(), myPanelName(), "MDM_plot");
  if(shapeExists("view_mdm"))
    removeSymbol(myModuleName(), myPanelName(), "view_mdm");
}

void stgBmon_remove_sensor() {

  if(shapeExists("Bxyz_plot"))
    removeSymbol(myModuleName(), myPanelName(), "Bxyz_plot");
  if(shapeExists("Bcal_plot"))
    removeSymbol(myModuleName(), myPanelName(), "Bcal_plot");
  if(shapeExists("temp_plot"))
    removeSymbol(myModuleName(), myPanelName(), "temp_plot");
  if(shapeExists("view_sensor"))
    removeSymbol(myModuleName(), myPanelName(), "view_sensor");
}

void stgBmon_remove_colormap_draw_sensor(string side, int sector, dyn_string params) {

  int k;
  string ref = side + sector;
  dyn_string sides = makeDynString("A", "C");

  if(shapeExists("view_colormap"))
    removeSymbol(myModuleName(), myPanelName(), "view_colormap");
  for(int i = 1; i <= 8; i++) {

    k = 2*i - 1;

    for(int j = 1; j <= 2; j++) {

      if(ref != sides[j] + k) {

        if(shapeExists("Colormap_LDW_" + sides[j] + k))
          removeSymbol(myModuleName(), myPanelName(), "Colormap_LDW_" + sides[j] + k);
      }
    }
  }

  stgBmon_draw_sensor(7, 378, params);
  if(shapeExists("Colormap_LDW_" + side + sector))
    removeSymbol(myModuleName(), myPanelName(), "Colormap_LDW_" + side + sector);
}

void stgBmon_remove_sector_draw_sensor(int sensor, dyn_string params) {

  if(shapeExists("view_sector"))
    removeSymbol(myModuleName(), myPanelName(), "view_sector");
  if(shapeExists("single_sector"))
    removeSymbol(myModuleName(), myPanelName(), "single_sector");

  for(int i = 1; i <= 12; i++) {

    if(i != sensor) {

      if(shapeExists("Wedge_sensor_" + i))
        removeSymbol(myModuleName(), myPanelName(), "Wedge_sensor_" + i);
    }
  }

  for(int j = 1; j <= 4; j++) {

    if(shapeExists("MDM_" + j))
      removeSymbol(myModuleName(), myPanelName(), "MDM_" + j);
  }

  stgBmon_draw_sensor(7, 378, params);

  if(shapeExists("Wedge_sensor_" + sensor))
    removeSymbol(myModuleName(), myPanelName(), "Wedge_sensor_" + sensor);
}

void stgBmon_remove_sector_draw_sensor_by_mdm(int node, dyn_string params) {

  if(shapeExists("view_sector"))
    removeSymbol(myModuleName(), myPanelName(), "view_sector");
  if(shapeExists("single_sector"))
    removeSymbol(myModuleName(), myPanelName(), "single_sector");

  for(int i = 1; i <= 12; i++) {

    if(shapeExists("Wedge_sensor_" + i))
      removeSymbol(myModuleName(), myPanelName(), "Wedge_sensor_" + i);
  }

  for(int j = 1; j <= 4; j++) {

    if(j != node) {

      if(shapeExists("MDM_" + j))
        removeSymbol(myModuleName(), myPanelName(), "MDM_" + j);
    }
  }

  stgBmon_draw_sensor(7, 378, params);
  if(shapeExists("MDM_" + node))
    removeSymbol(myModuleName(), myPanelName(), "MDM_" + node);
}

void stgBmon_remove_mdm_map_draw_mdm(dyn_string params) {

  stgBmon_draw_mdm(7, 387, params);
  stgBmon_remove_mdm_map();
}

void stgBmon_remove_mdm_draw_sensor(dyn_string params) {

  stgBmon_draw_sensor(7, 378, params);
  stgBmon_remove_mdm();
}

void stgBmon_remove_sector_draw_mdm(int node, dyn_string params) {

  if(shapeExists("view_sector"))
    removeSymbol(myModuleName(), myPanelName(), "view_sector");
  if(shapeExists("single_sector"))
    removeSymbol(myModuleName(), myPanelName(), "single_sector");

  for(int i = 1; i <= 12; i++) {

    if(shapeExists("Wedge_sensor_" + i))
      removeSymbol(myModuleName(), myPanelName(), "Wedge_sensor_" + i);
  }

  for(int j = 1; j <= 4; j++) {

    if(node != j) {
      if(shapeExists("MDM_" + j))
        removeSymbol(myModuleName(), myPanelName(), "MDM_" + j);
    }
  }

  stgBmon_draw_mdm(7, 387, params);
  if(shapeExists("MDM_" + node))
    removeSymbol(myModuleName(), myPanelName(), "MDM_" + node);
}
