//--------------------------------------------------------------------------------
/*
  @author Stamatios Tzanos, NTUA (stzanos: stamatios.tzanos@cern.ch)

  "stgBmon_commissioning.ctl" is a library for the commissioning of the
  bfield sensors on building 191.
*/

#uses "stgBmon/stgBmon_conversions.ctl"

void stgBmon_191_create_dpts() {

  int i, j;
  dyn_dyn_string msdpt;
  dyn_dyn_int tmsdpt;

  int n;
  dyn_errClass err;

  i=1;
  j=1;

  // Create the data type


  msdpt[i] = makeDynString ("191MDMSector","","","");

    msdpt[i=i+1] = makeDynString ("","name","","");

    msdpt[i=i+1] = makeDynString ("","node1","","");

      msdpt[i=i+1] = makeDynString ("","","id","");
      msdpt[i=i+1] = makeDynString ("","","effective","");
      msdpt[i=i+1] = makeDynString ("","","canport","");
      msdpt[i=i+1] = makeDynString ("","","portErr","");

        msdpt[i=i+1] = makeDynString ("","","data","");

          msdpt[i=i+1] = makeDynString ("","","","adc0");
          msdpt[i=i+1] = makeDynString ("","","","adc1");
          msdpt[i=i+1] = makeDynString ("","","","adc2");
          msdpt[i=i+1] = makeDynString ("","","","adc3");
          msdpt[i=i+1] = makeDynString ("","","","adc4");
          msdpt[i=i+1] = makeDynString ("","","","adc5");
          msdpt[i=i+1] = makeDynString ("","","","adc6");
          msdpt[i=i+1] = makeDynString ("","","","adc7");
          msdpt[i=i+1] = makeDynString ("","","","adc8");
          msdpt[i=i+1] = makeDynString ("","","","adc9");
          msdpt[i=i+1] = makeDynString ("","","","adc10");
          msdpt[i=i+1] = makeDynString ("","","","adc11");
          msdpt[i=i+1] = makeDynString ("","","","adc12");
          msdpt[i=i+1] = makeDynString ("","","","adc13");
          msdpt[i=i+1] = makeDynString ("","","","adc14");
          msdpt[i=i+1] = makeDynString ("","","","adc15");

    msdpt[i=i+1] = makeDynString ("","node2","","");

      msdpt[i=i+1] = makeDynString ("","","id","");
      msdpt[i=i+1] = makeDynString ("","","effective","");
      msdpt[i=i+1] = makeDynString ("","","canport","");
      msdpt[i=i+1] = makeDynString ("","","portErr","");

        msdpt[i=i+1] = makeDynString ("","","data","");

          msdpt[i=i+1] = makeDynString ("","","","adc0");
          msdpt[i=i+1] = makeDynString ("","","","adc1");
          msdpt[i=i+1] = makeDynString ("","","","adc2");
          msdpt[i=i+1] = makeDynString ("","","","adc3");
          msdpt[i=i+1] = makeDynString ("","","","adc4");
          msdpt[i=i+1] = makeDynString ("","","","adc5");
          msdpt[i=i+1] = makeDynString ("","","","adc6");
          msdpt[i=i+1] = makeDynString ("","","","adc7");
          msdpt[i=i+1] = makeDynString ("","","","adc8");
          msdpt[i=i+1] = makeDynString ("","","","adc9");
          msdpt[i=i+1] = makeDynString ("","","","adc10");
          msdpt[i=i+1] = makeDynString ("","","","adc11");
          msdpt[i=i+1] = makeDynString ("","","","adc12");
          msdpt[i=i+1] = makeDynString ("","","","adc13");
          msdpt[i=i+1] = makeDynString ("","","","adc14");
          msdpt[i=i+1] = makeDynString ("","","","adc15");

    msdpt[i=i+1] = makeDynString ("","node3","","");

      msdpt[i=i+1] = makeDynString ("","","id","");
      msdpt[i=i+1] = makeDynString ("","","effective","");
      msdpt[i=i+1] = makeDynString ("","","canport","");
      msdpt[i=i+1] = makeDynString ("","","portErr","");

        msdpt[i=i+1] = makeDynString ("","","data","");

          msdpt[i=i+1] = makeDynString ("","","","adc0");
          msdpt[i=i+1] = makeDynString ("","","","adc1");
          msdpt[i=i+1] = makeDynString ("","","","adc2");
          msdpt[i=i+1] = makeDynString ("","","","adc3");
          msdpt[i=i+1] = makeDynString ("","","","adc4");
          msdpt[i=i+1] = makeDynString ("","","","adc5");
          msdpt[i=i+1] = makeDynString ("","","","adc6");
          msdpt[i=i+1] = makeDynString ("","","","adc7");
          msdpt[i=i+1] = makeDynString ("","","","adc8");
          msdpt[i=i+1] = makeDynString ("","","","adc9");
          msdpt[i=i+1] = makeDynString ("","","","adc10");
          msdpt[i=i+1] = makeDynString ("","","","adc11");
          msdpt[i=i+1] = makeDynString ("","","","adc12");
          msdpt[i=i+1] = makeDynString ("","","","adc13");
          msdpt[i=i+1] = makeDynString ("","","","adc14");
          msdpt[i=i+1] = makeDynString ("","","","adc15");

    msdpt[i=i+1] = makeDynString ("","node4","","");

      msdpt[i=i+1] = makeDynString ("","","id","");
      msdpt[i=i+1] = makeDynString ("","","effective","");
      msdpt[i=i+1] = makeDynString ("","","canport","");
      msdpt[i=i+1] = makeDynString ("","","portErr","");

        msdpt[i=i+1] = makeDynString ("","","data","");

          msdpt[i=i+1] = makeDynString ("","","","adc0");
          msdpt[i=i+1] = makeDynString ("","","","adc1");
          msdpt[i=i+1] = makeDynString ("","","","adc2");
          msdpt[i=i+1] = makeDynString ("","","","adc3");
          msdpt[i=i+1] = makeDynString ("","","","adc4");
          msdpt[i=i+1] = makeDynString ("","","","adc5");
          msdpt[i=i+1] = makeDynString ("","","","adc6");
          msdpt[i=i+1] = makeDynString ("","","","adc7");
          msdpt[i=i+1] = makeDynString ("","","","adc8");
          msdpt[i=i+1] = makeDynString ("","","","adc9");
          msdpt[i=i+1] = makeDynString ("","","","adc10");
          msdpt[i=i+1] = makeDynString ("","","","adc11");
          msdpt[i=i+1] = makeDynString ("","","","adc12");
          msdpt[i=i+1] = makeDynString ("","","","adc13");
          msdpt[i=i+1] = makeDynString ("","","","adc14");
          msdpt[i=i+1] = makeDynString ("","","","adc15");




  //****************************************************//

  tmsdpt[j] = makeDynInt (DPEL_STRUCT);

    tmsdpt[j=j+1] = makeDynInt (0,DPEL_STRING); //name

    tmsdpt[j=j+1] = makeDynInt (0,DPEL_STRUCT); //node1

      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_UINT); //id
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING); //effective
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING); //canport
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_INT); //err

        tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRUCT); //data

          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT); //adc
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);

    tmsdpt[j=j+1] = makeDynInt (0,DPEL_STRUCT); //node2

      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_UINT); //id
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING); //effective
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING); //canport
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_INT); //err

        tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRUCT); //data

          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT); //adc
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);

    tmsdpt[j=j+1] = makeDynInt (0,DPEL_STRUCT); //node3

      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_UINT); //id
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING); //effective
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING); //canport
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_INT); //err

        tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRUCT); //data

          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT); //adc
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);

    tmsdpt[j=j+1] = makeDynInt (0,DPEL_STRUCT); //node4

      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_UINT); //id
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING); //effective
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING); //canport
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_INT); //err

        tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRUCT); //data

          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT); //adc
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);

  //************************************************//

  n = dpTypeChange(msdpt,tmsdpt);

  DebugN ("191MDMSector datapoint type created, result: ",n);

  if(n == 0) {
    DebugN("Etsi bravo");
  }

  err = getLastError();
    if(dynlen(err) > 0)
    {
       errorDialog(err);

       throwError(err);
    }
    else
    {
      DebugN("OK");
    }
}

void stgBmon_191_delete_dpts() {

  dpTypeDelete("191MDMSector");
}

void stgBmon_191_create_datapoints() {

  string prefix = "191EIZ4";
  dyn_string sector = makeDynString("1", "3", "5", "7", "9", "11", "13", "15");

  for(int nSector = 1; nSector <= 8; nSector++) {
    dpCreate(prefix + "A" + sector[nSector], "191MDMSector");
    dpCreate(prefix + "C" + sector[nSector], "191MDMSector");
  }
}

void stgBmon_191_delete_datapoints() {

  string prefix = "191EIZ4";
  dyn_string sector = makeDynString("1", "3", "5", "7", "9", "11", "13", "15");

  for(int nSector = 1; nSector <= 8; nSector++) {
    dpDelete(prefix + "A" + sector[nSector]);
    dpDelete(prefix + "C" + sector[nSector]);
  }
}

void stgBmon_191_create_upper_addressing_A() {

   //
  // ADDRESSING OF UPPER HALF OF THE WHEEL
  //

  string sys = getSystemName();

  // Values of the fwPeriphAddress
  string serverName = "OPCUACANOPENSERVER";
  int driver = 9;
  string opcKind = "1";
  string opcVariant = "1";
  int dataType = 0; // Default
  int mode = 2; // Input
  string opcSubscription = "bfield_data";
  string pollGroup = "";
  dyn_string exceptionInfo;
  bool setWait = TRUE;

  // OPC Items
  dyn_string opc_ids = makeDynString("ns=2;s=EIL4_A.NSW_S01_STG.NodeId",
                                     "ns=2;s=EIL4_A.NSW_S01_MMG.NodeId",
                                     "ns=2;s=EIL4_A.NSW_S02_STG.NodeId",
                                     "ns=2;s=EIL4_A.NSW_S02_MMG.NodeId",
                                     "ns=2;s=EIL4_A.NSW_S03_STG.NodeId",
                                     "ns=2;s=EIL4_A.NSW_S03_MMG.NodeId",
                                     "ns=2;s=EIL4_A.NSW_S04_STG.NodeId",
                                     "ns=2;s=EIL4_A.NSW_S04_MMG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S05_STG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S05_MMG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S06_STG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S06_MMG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S07_STG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S07_MMG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S08_STG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S08_MMG.NodeId");

  dyn_string opc_adcs = makeDynString("ns=2;s=EIL4_A.NSW_S01_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_A.NSW_S01_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_A.NSW_S02_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_A.NSW_S02_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_A.NSW_S03_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_A.NSW_S03_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_A.NSW_S04_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_A.NSW_S04_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S05_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S05_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S06_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S06_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S07_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S07_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S08_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S08_MMG.TPDO4.bfadc_");

  // DPs
  dyn_string dpe_ids = makeDynString(sys + "191EIZ4A1.node3.id",
                                     sys + "191EIZ4A1.node4.id",
                                     sys + "191EIZ4A3.node1.id",
                                     sys + "191EIZ4A3.node2.id",
                                     sys + "191EIZ4A3.node3.id",
                                     sys + "191EIZ4A3.node4.id",
                                     sys + "191EIZ4A5.node1.id",
                                     sys + "191EIZ4A5.node2.id",
                                     sys + "191EIZ4A5.node3.id",
                                     sys + "191EIZ4A5.node4.id",
                                     sys + "191EIZ4A7.node1.id",
                                     sys + "191EIZ4A7.node2.id",
                                     sys + "191EIZ4A7.node3.id",
                                     sys + "191EIZ4A7.node4.id",
                                     sys + "191EIZ4A9.node1.id",
                                     sys + "191EIZ4A9.node2.id");

  dyn_string dpe_adcs = makeDynString(sys + "191EIZ4A1.node3.data.adc",
                                     sys + "191EIZ4A1.node4.data.adc",
                                     sys + "191EIZ4A3.node1.data.adc",
                                     sys + "191EIZ4A3.node2.data.adc",
                                     sys + "191EIZ4A3.node3.data.adc",
                                     sys + "191EIZ4A3.node4.data.adc",
                                     sys + "191EIZ4A5.node1.data.adc",
                                     sys + "191EIZ4A5.node2.data.adc",
                                     sys + "191EIZ4A5.node3.data.adc",
                                     sys + "191EIZ4A5.node4.data.adc",
                                     sys + "191EIZ4A7.node1.data.adc",
                                     sys + "191EIZ4A7.node2.data.adc",
                                     sys + "191EIZ4A7.node3.data.adc",
                                     sys + "191EIZ4A7.node4.data.adc",
                                     sys + "191EIZ4A9.node1.data.adc",
                                     sys + "191EIZ4A9.node2.data.adc");


  for(int i = 1; i <= 16; i++) {

    fwPeriphAddress_setOPCUA(dpe_ids[i],
                                 serverName,
                                 driver,
                                 opc_ids[i],
                                 opcSubscription,
                                 opcKind,
                                 opcVariant,
                                 dataType,
                                 mode,
                                 pollGroup,
                                 exceptionInfo,
                                 setWait);

    for(int j = 0; j < 16; j++) {

      fwPeriphAddress_setOPCUA(dpe_adcs[i] + j,
                                 serverName,
                                 driver,
                                 opc_adcs[i] + j,
                                 opcSubscription,
                                 opcKind,
                                 opcVariant,
                                 dataType,
                                 mode,
                                 pollGroup,
                                 exceptionInfo,
                                 setWait);
    }
  }
}

void stgBmon_191_create_lower_addressing_A() {

   //
  // ADDRESSING OF LOWER HALF OF THE WHEEL
  //

  string sys = getSystemName();

  // Values of the fwPeriphAddress
  string serverName = "OPCUACANOPENSERVER";
  int driver = 9;
  string opcKind = "1";
  string opcVariant = "1";
  int dataType = 0; // Default
  int mode = 2; // Input
  string opcSubscription = "bfield_data";
  string pollGroup = "";
  dyn_string exceptionInfo;
  bool setWait = TRUE;

  // OPC Items
  dyn_string opc_ids = makeDynString("ns=2;s=EIL4_A.NSW_S09_STG.NodeId",
                                     "ns=2;s=EIL4_A.NSW_S09_MMG.NodeId",
                                     "ns=2;s=EIL4_A.NSW_S10_STG.NodeId",
                                     "ns=2;s=EIL4_A.NSW_S10_MMG.NodeId",
                                     "ns=2;s=EIL4_A.NSW_S11_STG.NodeId",
                                     "ns=2;s=EIL4_A.NSW_S11_MMG.NodeId",
                                     "ns=2;s=EIL4_A.NSW_S12_STG.NodeId",
                                     "ns=2;s=EIL4_A.NSW_S12_MMG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S13_STG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S13_MMG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S14_STG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S14_MMG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S15_MMG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S15_STG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S16_STG.NodeId",
                                     "ns=2;s=EIL5_A.NSW_S16_MMG.NodeId");

  dyn_string opc_adcs = makeDynString("ns=2;s=EIL4_A.NSW_S09_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_A.NSW_S09_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_A.NSW_S10_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_A.NSW_S10_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_A.NSW_S11_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_A.NSW_S11_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_A.NSW_S12_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_A.NSW_S12_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S13_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S13_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S14_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S14_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S15_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S15_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S16_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_A.NSW_S16_MMG.TPDO4.bfadc_");

  // DPs
  dyn_string dpe_ids = makeDynString(sys + "191EIZ4A9.node3.id",
                                     sys + "191EIZ4A9.node4.id",
                                     sys + "191EIZ4A11.node1.id",
                                     sys + "191EIZ4A11.node2.id",
                                     sys + "191EIZ4A11.node3.id",
                                     sys + "191EIZ4A11.node4.id",
                                     sys + "191EIZ4A13.node1.id",
                                     sys + "191EIZ4A13.node2.id",
                                     sys + "191EIZ4A13.node3.id",
                                     sys + "191EIZ4A13.node4.id",
                                     sys + "191EIZ4A15.node1.id",
                                     sys + "191EIZ4A15.node2.id",
                                     sys + "191EIZ4A15.node3.id",
                                     sys + "191EIZ4A15.node4.id",
                                     sys + "191EIZ4A1.node1.id",
                                     sys + "191EIZ4A1.node2.id");

  dyn_string dpe_adcs = makeDynString(sys + "191EIZ4A9.node3.data.adc",
                                     sys + "191EIZ4A9.node4.data.adc",
                                     sys + "191EIZ4A11.node1.data.adc",
                                     sys + "191EIZ4A11.node2.data.adc",
                                     sys + "191EIZ4A11.node3.data.adc",
                                     sys + "191EIZ4A11.node4.data.adc",
                                     sys + "191EIZ4A13.node1.data.adc",
                                     sys + "191EIZ4A13.node2.data.adc",
                                     sys + "191EIZ4A13.node3.data.adc",
                                     sys + "191EIZ4A13.node4.data.adc",
                                     sys + "191EIZ4A15.node1.data.adc",
                                     sys + "191EIZ4A15.node2.data.adc",
                                     sys + "191EIZ4A15.node3.data.adc",
                                     sys + "191EIZ4A15.node4.data.adc",
                                     sys + "191EIZ4A1.node1.data.adc",
                                     sys + "191EIZ4A1.node2.data.adc");


  for(int i = 1; i <= 16; i++) {

    fwPeriphAddress_setOPCUA(dpe_ids[i],
                                 serverName,
                                 driver,
                                 opc_ids[i],
                                 opcSubscription,
                                 opcKind,
                                 opcVariant,
                                 dataType,
                                 mode,
                                 pollGroup,
                                 exceptionInfo,
                                 setWait);

    for(int j = 0; j < 16; j++) {

      fwPeriphAddress_setOPCUA(dpe_adcs[i] + j,
                                 serverName,
                                 driver,
                                 opc_adcs[i] + j,
                                 opcSubscription,
                                 opcKind,
                                 opcVariant,
                                 dataType,
                                 mode,
                                 pollGroup,
                                 exceptionInfo,
                                 setWait);
    }
  }
}

void stgBmon_create_upper_addressing_C() {

  //
  // ADDRESSING OF UPPER HALF OF THE WHEEL
  //

  string sys = getSystemName();

  // Values of the fwPeriphAddress
  string serverName = "OPCUACANOPENSERVER";
  int driver = 9;
  string opcKind = "1";
  string opcVariant = "1";
  int dataType = 0; // Default
  int mode = 2; // Input
  string opcSubscription = "bfield_data";
  string pollGroup = "";
  dyn_string exceptionInfo;
  bool setWait = TRUE;

  // OPC Items
  dyn_string opc_ids = makeDynString("ns=2;s=EIL4_C.NSW_S01_STG.NodeId",
                                     "ns=2;s=EIL4_C.NSW_S01_MMG.NodeId",
                                     "ns=2;s=EIL4_C.NSW_S02_STG.NodeId",
                                     "ns=2;s=EIL4_C.NSW_S02_MMG.NodeId",
                                     "ns=2;s=EIL4_C.NSW_S03_STG.NodeId",
                                     "ns=2;s=EIL4_C.NSW_S03_MMG.NodeId",
                                     "ns=2;s=EIL4_C.NSW_S04_STG.NodeId",
                                     "ns=2;s=EIL4_C.NSW_S04_MMG.NodeId",
                                     "ns=2;s=EIL5_C.NSW_S05_STG.NodeId",
                                     "ns=2;s=EIL5_C.NSW_S05_MMG.NodeId",
                                     "ns=2;s=EIL5_C.NSW_S06_STG.NodeId",
                                     "ns=2;s=EIL5_C.NSW_S06_MMG.NodeId",
                                     "ns=2;s=EIL5_C.NSW_S07_STG.NodeId",
                                     "ns=2;s=EIL5_C.NSW_S07_MMG.NodeId",
                                     "ns=2;s=EIL5_C.NSW_S08_STG.NodeId",
                                     "ns=2;s=EIL5_C.NSW_S08_MMG.NodeId");

  dyn_string opc_adcs = makeDynString("ns=2;s=EIL4_C.NSW_S01_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_C.NSW_S01_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_C.NSW_S02_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_C.NSW_S02_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_C.NSW_S03_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_C.NSW_S03_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_C.NSW_S04_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_C.NSW_S04_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_C.NSW_S05_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_C.NSW_S05_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_C.NSW_S06_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_C.NSW_S06_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_C.NSW_S07_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_C.NSW_S07_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_C.NSW_S08_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_C.NSW_S08_MMG.TPDO4.bfadc_");

  // DPs
  dyn_string dpe_ids = makeDynString(sys + "191EIZ4C1.node3.id",
                                     sys + "191EIZ4C1.node4.id",
                                     sys + "191EIZ4C3.node1.id",
                                     sys + "191EIZ4C3.node2.id",
                                     sys + "191EIZ4C3.node3.id",
                                     sys + "191EIZ4C3.node4.id",
                                     sys + "191EIZ4C5.node1.id",
                                     sys + "191EIZ4C5.node2.id",
                                     sys + "191EIZ4C5.node3.id",
                                     sys + "191EIZ4C5.node4.id",
                                     sys + "191EIZ4C7.node1.id",
                                     sys + "191EIZ4C7.node2.id",
                                     sys + "191EIZ4C7.node3.id",
                                     sys + "191EIZ4C7.node4.id",
                                     sys + "191EIZ4C9.node1.id",
                                     sys + "191EIZ4C9.node2.id");

  dyn_string dpe_adcs = makeDynString(sys + "191EIZ4C1.node3.data.adc",
                                     sys + "191EIZ4C1.node4.data.adc",
                                     sys + "191EIZ4C3.node1.data.adc",
                                     sys + "191EIZ4C3.node2.data.adc",
                                     sys + "191EIZ4C3.node3.data.adc",
                                     sys + "191EIZ4C3.node4.data.adc",
                                     sys + "191EIZ4C5.node1.data.adc",
                                     sys + "191EIZ4C5.node2.data.adc",
                                     sys + "191EIZ4C5.node3.data.adc",
                                     sys + "191EIZ4C5.node4.data.adc",
                                     sys + "191EIZ4C7.node1.data.adc",
                                     sys + "191EIZ4C7.node2.data.adc",
                                     sys + "191EIZ4C7.node3.data.adc",
                                     sys + "191EIZ4C7.node4.data.adc",
                                     sys + "191EIZ4C9.node1.data.adc",
                                     sys + "191EIZ4C9.node2.data.adc");


  for(int i = 1; i <= 16; i++) {

    fwPeriphAddress_setOPCUA(dpe_ids[i],
                                 serverName,
                                 driver,
                                 opc_ids[i],
                                 opcSubscription,
                                 opcKind,
                                 opcVariant,
                                 dataType,
                                 mode,
                                 pollGroup,
                                 exceptionInfo,
                                 setWait);

    for(int j = 0; j < 16; j++) {

      fwPeriphAddress_setOPCUA(dpe_adcs[i] + j,
                                 serverName,
                                 driver,
                                 opc_adcs[i] + j,
                                 opcSubscription,
                                 opcKind,
                                 opcVariant,
                                 dataType,
                                 mode,
                                 pollGroup,
                                 exceptionInfo,
                                 setWait);
    }
  }
}

void stgBmon_create_lower_addressing_C() {

  //
  // ADDRESSING OF LOWER HALF OF THE WHEEL
  //

  string sys = getSystemName();

  // Values of the fwPeriphAddress
  string serverName = "OPCUACANOPENSERVER_C";
  int driver = 10;
  string opcKind = "1";
  string opcVariant = "1";
  int dataType = 0; // Default
  int mode = 2; // Input
  string opcSubscription = "bfield_data_C";
  string pollGroup = "";
  dyn_string exceptionInfo;
  bool setWait = TRUE;

  // OPC Items
  dyn_string opc_ids = makeDynString("ns=2;s=EIL4_C.NSW_S09_STG.NodeId",
                                     "ns=2;s=EIL4_C.NSW_S09_MMG.NodeId",
                                     "ns=2;s=EIL4_C.NSW_S10_STG.NodeId",
                                     "ns=2;s=EIL4_C.NSW_S10_MMG.NodeId",
                                     "ns=2;s=EIL4_C.NSW_S11_STG.NodeId",
                                     "ns=2;s=EIL4_C.NSW_S11_MMG.NodeId",
                                     "ns=2;s=EIL4_C.NSW_S12_STG.NodeId",
                                     "ns=2;s=EIL4_C.NSW_S12_MMG.NodeId",
                                     "ns=2;s=EIL5_C.NSW_S13_STG.NodeId",
                                     "ns=2;s=EIL5_C.NSW_S13_MMG.NodeId",
                                     "ns=2;s=EIL5_C.NSW_S14_STG.NodeId",
                                     "ns=2;s=EIL5_C.NSW_S14_MMG.NodeId",
                                     "ns=2;s=EIL5_C.NSW_S15_MMG.NodeId",
                                     "ns=2;s=EIL5_C.NSW_S15_STG.NodeId",
                                     "ns=2;s=EIL5_C.NSW_S16_STG.NodeId",
                                     "ns=2;s=EIL5_C.NSW_S16_MMG.NodeId");

  dyn_string opc_adcs = makeDynString("ns=2;s=EIL4_C.NSW_S09_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_C.NSW_S09_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_C.NSW_S10_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_C.NSW_S10_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_C.NSW_S11_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_C.NSW_S11_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_C.NSW_S12_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL4_C.NSW_S12_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_C.NSW_S13_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_C.NSW_S13_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_C.NSW_S14_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_C.NSW_S14_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_C.NSW_S15_MMG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_C.NSW_S15_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_C.NSW_S16_STG.TPDO4.bfadc_",
                                     "ns=2;s=EIL5_C.NSW_S16_MMG.TPDO4.bfadc_");

  // DPs
  dyn_string dpe_ids = makeDynString(sys + "191EIZ4C9.node3.id",
                                     sys + "191EIZ4C9.node4.id",
                                     sys + "191EIZ4C11.node1.id",
                                     sys + "191EIZ4C11.node2.id",
                                     sys + "191EIZ4C11.node3.id",
                                     sys + "191EIZ4C11.node4.id",
                                     sys + "191EIZ4C13.node1.id",
                                     sys + "191EIZ4C13.node2.id",
                                     sys + "191EIZ4C13.node3.id",
                                     sys + "191EIZ4C13.node4.id",
                                     sys + "191EIZ4C15.node1.id",
                                     sys + "191EIZ4C15.node2.id",
                                     sys + "191EIZ4C15.node3.id",
                                     sys + "191EIZ4C15.node4.id",
                                     sys + "191EIZ4C1.node1.id",
                                     sys + "191EIZ4C1.node2.id");

  dyn_string dpe_adcs = makeDynString(sys + "191EIZ4C9.node3.data.adc",
                                     sys + "191EIZ4C9.node4.data.adc",
                                     sys + "191EIZ4C11.node1.data.adc",
                                     sys + "191EIZ4C11.node2.data.adc",
                                     sys + "191EIZ4C11.node3.data.adc",
                                     sys + "191EIZ4C11.node4.data.adc",
                                     sys + "191EIZ4C13.node1.data.adc",
                                     sys + "191EIZ4C13.node2.data.adc",
                                     sys + "191EIZ4C13.node3.data.adc",
                                     sys + "191EIZ4C13.node4.data.adc",
                                     sys + "191EIZ4C15.node1.data.adc",
                                     sys + "191EIZ4C15.node2.data.adc",
                                     sys + "191EIZ4C15.node3.data.adc",
                                     sys + "191EIZ4C15.node4.data.adc",
                                     sys + "191EIZ4C1.node1.data.adc",
                                     sys + "191EIZ4C1.node2.data.adc");


  for(int i = 1; i <= 16; i++) {

    fwPeriphAddress_setOPCUA(dpe_ids[i],
                                 serverName,
                                 driver,
                                 opc_ids[i],
                                 opcSubscription,
                                 opcKind,
                                 opcVariant,
                                 dataType,
                                 mode,
                                 pollGroup,
                                 exceptionInfo,
                                 setWait);

    for(int j = 0; j < 16; j++) {

      fwPeriphAddress_setOPCUA(dpe_adcs[i] + j,
                                 serverName,
                                 driver,
                                 opc_adcs[i] + j,
                                 opcSubscription,
                                 opcKind,
                                 opcVariant,
                                 dataType,
                                 mode,
                                 pollGroup,
                                 exceptionInfo,
                                 setWait);
    }
  }
}

void stgBmon_191_draw_MDMs(string side, int sector) {

  int mask;

  // Draw tables
  dyn_float x_table = makeDynString(30, 450, 30, 450);
  dyn_float y_table = makeDynString(505, 505, 730, 730);

  // Draw MDMs
  dyn_float x_MDM = makeDynString(100, 520, 100, 520);
  dyn_float y_MDM = makeDynString(455, 455, 680, 680);

  // Draw MDM infos (v2)
  dyn_float x_MDM_info = makeDynString(170, 590, 170, 590);
  dyn_float y_MDM_info = makeDynString(415, 415, 640, 640);

  // Draw MDM infos
  //dyn_float x_MDM_info = makeDynString(190, 610, 190, 610);
  //dyn_float y_MDM_info = makeDynString(405, 405, 630, 630);

  // Draw adc values
  float x_adc_1 = 120;
  float x_adc_2 = 540;
  float y_adc_1 = 535;
  float y_adc_2 = 760;
  float x_adc_off = 70;
  float y_adc_off = 20;
  dyn_float x_adc = makeDynString(x_adc_1, x_adc_2, x_adc_1, x_adc_2);
  dyn_float y_adc = makeDynString(y_adc_1, y_adc_1, y_adc_2, y_adc_2);
  dyn_float x_adc_offset = makeDynString(0, x_adc_off, 2*x_adc_off, 3*x_adc_off);
  dyn_float y_adc_offset = makeDynString(0, y_adc_off, 2*y_adc_off, 3*y_adc_off);

  for(int j = 1; j <= 4; j++) {

    addSymbol(myModuleName(), myPanelName(),
              "objects/stgBmon/stgBmonCommissioning191/STG_191_ADC_TABLE.xml",
              "adc_table" + j, "",
              x_table[j], y_table[j], 0,
              0.65, 0.65);

    mask = stgBmon_nodeToMaskInt(j);
    addSymbol(myModuleName(), myPanelName(),
              "objects/stgBmon/stgBmonCommissioning191/STG_191_MDM.pnl.xml",
              "MDM" + j, makeDynString("$side:" + side, "$sector:" + sector, "$node:" + j, "$mask:" + mask),
              x_MDM[j], y_MDM[j], 0,
              0.6, 0.6);

    addSymbol(myModuleName(), myPanelName(),
              "objects/stgBmon/stgBmonCommissioning191/STG_191_MDM_INFO_v2.pnl.xml",
              "MDM_INFO" + j, makeDynString("$node:" + j, "$mask:" + mask),
              x_MDM_info[j], y_MDM_info[j], 0,
              1, 1);

    int adc_counter = 0;
    for(int a = 1; a <= 4; a++) {
      for(int b = 1; b <= 4; b++) {
        addSymbol(myModuleName(), myPanelName(),
              "objects/stgBmon/stgBmonCommissioning191/STG_191_ADC.pnl.xml",
              "adc" + (string)j + (string)adc_counter, makeDynString("$side:" + side,
                                                                     "$sector:" + sector,
                                                                     "$node:" + j,
                                                                     "$adc_field:" + adc_counter),
              x_adc[j] + x_adc_offset[b], y_adc[j] + y_adc_offset[a], 0,
              0.6, 0.6);
        adc_counter++;
      }
    }
  }
}

void stgBmon_191_remove_MDMs() {

  for(int j = 1; j <= 4; j++) {

    if(shapeExists("adc_table" + j))
      removeSymbol(myModuleName(), myPanelName(), "adc_table" + j);
    if(shapeExists("MDM" + j))
      removeSymbol(myModuleName(), myPanelName(), "MDM" + j);
    if(shapeExists("MDM_INFO" + j))
      removeSymbol(myModuleName(), myPanelName(), "MDM_INFO" + j);

    int adc_counter = 0;
    for(int a = 1; a <= 4; a++) {
      for(int b = 1; b <= 4; b++) {
        if(shapeExists("adc" + (string)j + (string)adc_counter))
          removeSymbol(myModuleName(), myPanelName(), "adc" + (string)j + (string)adc_counter);
        adc_counter++;
      }
    }
  }
}

void stgBmon_191_draw_sectors_A() {

  // Side A
  float x_center_A = 215;
  float y_center_A = 203;
  float R = 60;
  float rho = 140;

  dyn_float phi_A = makeDynFloat(-90, -45, 0, 45, 90, 135, 180, 225);
  dyn_float x_A, x_C, x_sensor_A, x_sensor_C;
  dyn_float y_A, y_C, y_sensor_A, y_sensor_C;

  string wedge_A, wedge_C;
  int k;

  for(int i = 1; i <= 8; i++) {

    x_A[i] = x_center_A - R*sin(deg2rad(phi_A[i]));
    y_A[i] = y_center_A - R*cos(deg2rad(phi_A[i]));
    x_sensor_A[i] = x_center_A - rho*sin(deg2rad(phi_A[i]));
    y_sensor_A[i] = y_center_A - rho*cos(deg2rad(phi_A[i]));

    k = 2*i - 1;

    // Ref: LDW_A#
    addSymbol(myModuleName(), myPanelName(),
            "objects/stgBmon/stgBmonCommissioning191/STG_191_SECTOR.pnl.xml",
            "LDW_A" + k, makeDynString("$side:" + "A", "$sector:" + k),
            x_A[i], y_A[i], phi_A[i],
            0.60, 0.60);

    delay(0, 40);
  }
}

void stgBmon_191_draw_sectors_C() {

  // Side A
  float x_center_C = 215;
  float y_center_C = 203;
  float R = 60;
  float rho = 140;

  dyn_float phi_A = makeDynFloat(-90, -45, 0, 45, 90, 135, 180, 225);
  dyn_float phi_C = makeDynFloat(90, 45, 0, -45, -90, -135, -180, -225);
  dyn_float x_A, x_C, x_sensor_A, x_sensor_C;
  dyn_float y_A, y_C, y_sensor_A, y_sensor_C;

  string wedge_A, wedge_C;
  int k;

  for(int i = 1; i <= 8; i++) {

    x_C[i] = x_center_C - R*sin(deg2rad(phi_C[i]));
    y_C[i] = y_center_C - R*cos(deg2rad(phi_C[i]));
    x_sensor_C[i] = x_center_C - rho*sin(deg2rad(phi_C[i]));
    y_sensor_C[i] = y_center_C - rho*cos(deg2rad(phi_C[i]));

    k = 2*i - 1;

    // Ref: LDW_C#
    addSymbol(myModuleName(), myPanelName(),
            "objects/stgBmon/stgBmonCommissioning191/STG_191_SECTOR.pnl.xml",
            "LDW_C" + k, makeDynString("$side:" + "C", "$sector:" + k),
            x_C[i], y_C[i], phi_C[i],
            0.60, 0.60);

    delay(0, 40);
  }
}
