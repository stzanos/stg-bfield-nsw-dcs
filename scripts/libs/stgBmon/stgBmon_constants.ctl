//--------------------------------------------------------------------------------
/*
  @author Stamatios Tzanos, NTUA (stzanos: stamatios.tzanos@cern.ch)

  "stgBmon_constants.ctl" is a library of all the constants that are used
  in the sTGC BField sensor monitoring project. The live server data is stored
  in the appropriate datapoints in the format 'EIZ(1/2)(A/C)(sector number)'."
*/

// B-Sensor states
const  int   BSENSOR_OKAY    = 0;
const  int   BSENSOR_DEAD    = 1;
const  int   BSENSOR_BAD     = 2;
const  int   BSENSOR_EMG     = 3;
const  int   BSENSOR_TEMP    = 4;
const  int   BSENSOR_MASK    = 5;
const  int   BSENSOR_UNK     = 6;
const  int   BSENSOR_NOID    = 7;

// B-Sensor color codes
const string BSENSOR_GREEN_1  = "{0, 250, 0}";
const string BSENSOR_GREEN_2  = "{25, 250, 0}";
const string BSENSOR_GREEN_3  = "{50, 250, 0}";
const string BSENSOR_GREEN_4  = "{75, 250, 0}";
const string BSENSOR_GREEN_5  = "{100, 250, 0}";
const string BSENSOR_GREEN_6  = "{125, 250, 0}";
const string BSENSOR_GREEN_7  = "{150, 250, 0}";
const string BSENSOR_GREEN_8  = "{175, 250, 0}";
const string BSENSOR_GREEN_9  = "{200, 250, 0}";
const string BSENSOR_YELLOW   = "{250, 250, 0}"; // Warning
const string BSENSOR_ORANGE   = "{250, 150, 0}"; // Error
const string BSENSOR_RED      = "{250, 0, 0}"; // Fatal/Dead/Bad
const string BSENSOR_GREY     = "{200, 200, 200}"; // Disconnected/Masked

// B-Sensor colormap ranges
// TO BE UPDATED WITH REALISTIC VALUES
const float BSENSOR_GOOD_1 = 3.25;
const float BSENSOR_GOOD_2 = 3.50;
const float BSENSOR_GOOD_3 = 3.75;
const float BSENSOR_GOOD_4 = 4.00;
const float BSENSOR_GOOD_5 = 4.25;
const float BSENSOR_GOOD_6 = 4.50;
const float BSENSOR_GOOD_7 = 4.75;
const float BSENSOR_GOOD_8 = 5.00;
const float BSENSOR_GOOD_9 = 5.25;
const float BSENSOR_WARNING = 5.75;


// Return the BField project name
string stgBmon_returnProjectName() {

  return getSystemName();
  // return "ATLMUONSWMDM:";
}

// B-Sensor colormap ranges
string stgBmon_returnColor(string state, float bfield) {

  if(state == BSENSOR_OKAY) {
    if(bfield <= BSENSOR_GOOD_1) return BSENSOR_GREEN_1;
    else if(bfield <= BSENSOR_GOOD_2) return BSENSOR_GREEN_2;
    else if(bfield <= BSENSOR_GOOD_3) return BSENSOR_GREEN_3;
    else if(bfield <= BSENSOR_GOOD_4) return BSENSOR_GREEN_4;
    else if(bfield <= BSENSOR_GOOD_5) return BSENSOR_GREEN_5;
    else if(bfield <= BSENSOR_GOOD_6) return BSENSOR_GREEN_6;
    else if(bfield <= BSENSOR_GOOD_7) return BSENSOR_GREEN_7;
    else if(bfield <= BSENSOR_GOOD_8) return BSENSOR_GREEN_8;
    else if(bfield <= BSENSOR_GOOD_9) return BSENSOR_GREEN_9;
    else if(bfield <= BSENSOR_WARNING) return BSENSOR_YELLOW;
    else return BSENSOR_ORANGE;
  }
  else if(state == BSENSOR_DEAD || state == BSENSOR_BAD)
    return BSENSOR_RED;
  else if(state == BSENSOR_MASK)
    return BSENSOR_GREY;
  else
    return "White";
}

// Side on spoke for dpNames("EIZ*" + side + "*") mapping.
dyn_int stgBmon_spokeSides() {

  return makeDynInt(3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 2, 3, 2, // STG
                    4, 2, 4, 2, 4, 2, 4, 2, 4, 2, 4, 2, 4, 1, 4, 1); // MMG
}

// Large sectors for dpNames("EIZ*" + side + "*") mapping.
dyn_int stgBmon_largeSectors() {

  return makeDynInt(1, 3, 3, 5, 5, 7, 7, 9, 9, 11, 11, 13, 13, 15, 15, 1, // STG
                    1, 3, 3, 5, 5, 7, 7, 9, 9, 11, 11, 13, 13, 15, 15, 1); // MMG
}


// BfMasks for dpNames("EIZ*" + side + "*") mapping.
dyn_int stgBmon_nodeMasks() {

  return makeDynInt(3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 16, 3, 16, // STG
                    16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 3, 16, 3); // MMG
}

// Side A node IDs for dpNames("EIZ*" + side + "*") mapping.
dyn_int stgBmon_sideA_nodes() {

  return makeDynInt(116, 43, 39, 126, 102, 115, 107, 37, 91, 22, 85, 31, 70, 74, 32, 39, // STG
                    40, 46, 28, 24, 45, 121, 66, 31, 49, 55, 47, 89, 111, 122, 125, 113); // MMG
}

// Side C node IDs for dpNames("EIZ*" + side + "*") mapping.
dyn_int stgBmon_sideC_nodes() {

  return makeDynInt(21, 34, 60, 119, 127, 104, 58, 108, 113, 27, 92, 30, 96, 44, 23, 41, // STG
                    127, 123, 100, 79, 60, 56, 21, 50, 21, 54, 3, 16, 109, 12, 56, 98); // MMG
}
