//--------------------------------------------------------------------------------
/*
  @author Stamatios Tzanos, NTUA (stzanos: stamatios.tzanos@cern.ch)
*/

// -------------------------
// Number of sensors connected on MDM
// -------------------------

string sys = getSystemName();

int stgBmon_bsensorNum() {

  int adc;
  int n = 16;

  for(int i = 0; i < 16; i++) {

    dpGet(sys + "EIZ4ALocal.node0.bf.data.adc" + i + ":_online.._value", adc);

    if(adc == 0) {
      n--;
    }

  }

  return n/4;
}

// -------------------------
// Set the colour of a shape
// -------------------------
void stgBmon_setBackCol(string graphicalObject, string colour) {


  setValue(graphicalObject, "backCol", colour);
}

// -------------------------
// Returns the colormap of the MDM in array according to bfMask
// -------------------------
dyn_string stgBmon_mdmSensorColorMap(int bfMask) {

  if(bfMask == 0) {
    return makeDynString("White", "White", "White", "White");
  }
  else if(bfMask == 1) {
    return makeDynString("Red", "White", "White", "White");
  }
  else if(bfMask == 2) {
    return makeDynString("White", "Red", "White", "White");
  }
  else if(bfMask == 3) {
    return makeDynString("Red", "Red", "White", "White");
  }
  else if(bfMask == 5) {
    return makeDynString("Red", "White", "Red", "White");
  }
  else if(bfMask == 7) {
    return makeDynString("Red", "Red", "Red", "White");
  }
  else if(bfMask == 10) {
    return makeDynString("White", "Red", "White", "Red");
  }
  else if(bfMask == 11) {
    return makeDynString("Red", "Red", "White", "Red");
  }
  else if(bfMask == 16) {
    return makeDynString("Red", "Red", "Red", "Red");
  }
}

// -------------------------
// Set MDM colormap depending on the bfMask
// -------------------------
void stgBmon_setMdmSensorColorMap(int bfMask) {

  dyn_string cols = stgBmon_mdmSensorColorMap(bfMask);

  for(int i = 0; i < 4; i++) {

    stgBmon_setBackCol("sensor" + i, cols[i+1]);
  }
}

// -------------------------
// Reset graphical objects' colours
// -------------------------
void stgBmon_resetMdmSensorColorMap() {

   for(int i = 0; i < 4; i++) {

    stgBmon_setBackCol("sensor" + i, "White");
  }
}

// -------------------------
// Returns the sensor names connected on MDM
// -------------------------
void stgBmon_updateMdmSensorColorMap() {

  dyn_bool connectedMap = stgBmon_sensorsConnectedOnMdm();

  for(int i = 0; i < 4; i++) {

    if(connectedMap[i + 1]) {

        setValue("sensor" + i, "backCol", "Green");
    }
  }
}

// -------------------------
// Check if a sensor ("sensor0", "sensor1", ... ) is connected by its ID
// -------------------------
bool stgBmon_sensorConnected_byID(string sensor) {

  int id;
  dpGet(sys + "Test.bf." + sensor + ".idhigh:_online.._value", id);

  if(id != 0) {

    dpSet(sys + "Test.bf." + sensor + ".state", TRUE);
    return TRUE;
  }
  else {
    dpSet(sys + "Test.bf." + sensor + ".state:", FALSE);
    return FALSE;
  }
}

// -------------------------
// Return an array of which positions are connected
// -------------------------
dyn_bool stgBmon_sensorsConnectedOnMdm() {

  dyn_bool sensorsBool = makeDynBool(FALSE, FALSE, FALSE, FALSE);

  for(int i = 0; i < 4; i++) {

    sensorsBool[i + 1] = stgBmon_sensorConnected_byID("sensor" + i);
  }

  return sensorsBool;
}

// -------------------------
// Displays sensor IDs on the textfield
// -------------------------
void stgBmon_setSensorIDsOnPanel() {

  string sensor_id;

  for(int i = 0; i < 4; i++) {

    dpGet(sys + "Test.bf.sensor" + i + ".id:_online.._value", sensor_id);
    setValue("sensor" + i + "id", "text", sensor_id);
  }
}

bool stgBmon_checkTestConformity() {

  int bfMask, sensorNum;

  dpGet(sys + "Test.bf.bfMask:_online.._value", bfMask);
  dpGet(sys + "Test.numberOfBsensors:_online.._value", sensorNum);

  if(bfMask == 1 && sensorNum == 1) {

    return TRUE;
  }
  else if(bfMask == 5 && sensorNum == 2) {

    return TRUE;
  }
  else {

    return FALSE;
  }

}

void stgBmon_createTestingDpt() {

  int i, j;

  dyn_dyn_string msdpt;
  dyn_dyn_int tmsdpt;

  int n;

  dyn_errClass err;

  i = 1;
  j = 1;

  msdpt[i] = makeDynString("Bfield_Monitor",  "", "", "");

    msdpt[i = i + 1] = makeDynString("",  "state", "", "");
    msdpt[i = i + 1] = makeDynString("",  "numberOfBsensors", "", "");

    msdpt[i = i + 1] = makeDynString("",  "node", "", "");

      msdpt[i = i + 1] = makeDynString("", "", "id", "");
      msdpt[i = i + 1] = makeDynString("", "", "type", "");
      msdpt[i = i + 1] = makeDynString("", "", "canbus", "");
      msdpt[i = i + 1] = makeDynString("", "", "portErr", "");
      msdpt[i = i + 1] = makeDynString("", "", "busid", "");
      msdpt[i = i + 1] = makeDynString("", "", "state", "");

    msdpt[i = i + 1] = makeDynString("", "bf", "", "");

      msdpt[i = i + 1] = makeDynString("", "", "bfMask", "");

      msdpt[i = i + 1] = makeDynString("", "", "data", "");

        msdpt[i = i + 1] = makeDynString("", "", "", "adc0");
        msdpt[i = i + 1] = makeDynString("", "", "", "adc1");
        msdpt[i = i + 1] = makeDynString("", "", "", "adc2");
        msdpt[i = i + 1] = makeDynString("", "", "", "adc3");
        msdpt[i = i + 1] = makeDynString("", "", "", "adc4");
        msdpt[i = i + 1] = makeDynString("", "", "", "adc5");
        msdpt[i = i + 1] = makeDynString("", "", "", "adc6");
        msdpt[i = i + 1] = makeDynString("", "", "", "adc7");
        msdpt[i = i + 1] = makeDynString("", "", "", "adc8");
        msdpt[i = i + 1] = makeDynString("", "", "", "adc9");
        msdpt[i = i + 1] = makeDynString("", "", "", "adc10");
        msdpt[i = i + 1] = makeDynString("", "", "", "adc11");
        msdpt[i = i + 1] = makeDynString("", "", "", "adc12");
        msdpt[i = i + 1] = makeDynString("", "", "", "adc13");
        msdpt[i = i + 1] = makeDynString("", "", "", "adc14");
        msdpt[i = i + 1] = makeDynString("", "", "", "adc15");

        msdpt[i = i + 1] = makeDynString("", "", "", "conf0");
        msdpt[i = i + 1] = makeDynString("", "", "", "conf1");
        msdpt[i = i + 1] = makeDynString("", "", "", "conf2");
        msdpt[i = i + 1] = makeDynString("", "", "", "conf3");
        msdpt[i = i + 1] = makeDynString("", "", "", "conf4");
        msdpt[i = i + 1] = makeDynString("", "", "", "conf5");
        msdpt[i = i + 1] = makeDynString("", "", "", "conf6");
        msdpt[i = i + 1] = makeDynString("", "", "", "conf7");
        msdpt[i = i + 1] = makeDynString("", "", "", "conf8");
        msdpt[i = i + 1] = makeDynString("", "", "", "conf9");
        msdpt[i = i + 1] = makeDynString("", "", "", "conf10");
        msdpt[i = i + 1] = makeDynString("", "", "", "conf11");
        msdpt[i = i + 1] = makeDynString("", "", "", "conf12");
        msdpt[i = i + 1] = makeDynString("", "", "", "conf13");
        msdpt[i = i + 1] = makeDynString("", "", "", "conf14");
        msdpt[i = i + 1] = makeDynString("", "", "", "conf15");

      msdpt[i = i + 1] = makeDynString("", "", "sensor0", "");

        msdpt[i = i + 1] = makeDynString("", "", "", "id");
        msdpt[i = i + 1] = makeDynString("", "", "", "idhigh");
        msdpt[i = i + 1] = makeDynString("", "", "", "idlow");
        msdpt[i = i + 1] = makeDynString("", "", "", "h1");
        msdpt[i = i + 1] = makeDynString("", "", "", "h2");
        msdpt[i = i + 1] = makeDynString("", "", "", "h3");
        msdpt[i = i + 1] = makeDynString("", "", "", "temp");
        msdpt[i = i + 1] = makeDynString("", "", "", "state");

      msdpt[i = i + 1] = makeDynString("", "", "sensor1", "");

        msdpt[i = i + 1] = makeDynString("", "", "", "id");
        msdpt[i = i + 1] = makeDynString("", "", "", "idhigh");
        msdpt[i = i + 1] = makeDynString("", "", "", "idlow");
        msdpt[i = i + 1] = makeDynString("", "", "", "h1");
        msdpt[i = i + 1] = makeDynString("", "", "", "h2");
        msdpt[i = i + 1] = makeDynString("", "", "", "h3");
        msdpt[i = i + 1] = makeDynString("", "", "", "temp");
        msdpt[i = i + 1] = makeDynString("", "", "", "state");

      msdpt[i = i + 1] = makeDynString("", "", "sensor2", "");

        msdpt[i = i + 1] = makeDynString("", "", "", "id");
        msdpt[i = i + 1] = makeDynString("", "", "", "idhigh");
        msdpt[i = i + 1] = makeDynString("", "", "", "idlow");
        msdpt[i = i + 1] = makeDynString("", "", "", "h1");
        msdpt[i = i + 1] = makeDynString("", "", "", "h2");
        msdpt[i = i + 1] = makeDynString("", "", "", "h3");
        msdpt[i = i + 1] = makeDynString("", "", "", "temp");
        msdpt[i = i + 1] = makeDynString("", "", "", "state");

      msdpt[i = i + 1] = makeDynString("", "", "sensor3", "");

        msdpt[i = i + 1] = makeDynString("", "", "", "id");
        msdpt[i = i + 1] = makeDynString("", "", "", "idhigh");
        msdpt[i = i + 1] = makeDynString("", "", "", "idlow");
        msdpt[i = i + 1] = makeDynString("", "", "", "h1");
        msdpt[i = i + 1] = makeDynString("", "", "", "h2");
        msdpt[i = i + 1] = makeDynString("", "", "", "h3");
        msdpt[i = i + 1] = makeDynString("", "", "", "temp");
        msdpt[i = i + 1] = makeDynString("", "", "", "state");


  // ----------------------------------------------------------------------//


  tmsdpt[j] = makeDynInt(DPEL_STRUCT); //Bfield_test

    tmsdpt[j = j + 1] = makeDynInt(0, DPEL_BOOL); //state
    tmsdpt[j = j + 1] = makeDynInt(0, DPEL_INT); //numbOfBsensrs

    tmsdpt[j = j + 1] = makeDynInt(0, DPEL_STRUCT); //node

      tmsdpt[j = j + 1] = makeDynInt(0, 0, DPEL_INT); //id
      tmsdpt[j = j + 1] = makeDynInt(0, 0, DPEL_STRING); //type
      tmsdpt[j = j + 1] = makeDynInt(0, 0, DPEL_STRING); //canbus
      tmsdpt[j = j + 1] = makeDynInt(0, 0, DPEL_BOOL); //portErr
      tmsdpt[j = j + 1] = makeDynInt(0, 0, DPEL_STRING); //busid
      tmsdpt[j = j + 1] = makeDynInt(0, 0, DPEL_BOOL); //state

    tmsdpt[j = j + 1] = makeDynInt(0, DPEL_STRUCT); //bf

      tmsdpt[j = j + 1] = makeDynInt(0, 0, DPEL_INT); //bfMask

      tmsdpt[j = j + 1] = makeDynInt(0, 0, DPEL_STRUCT); //data

        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT); //adc
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT);

        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_BIT32); //conf
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_BIT32);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_BIT32);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_BIT32);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_BIT32);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_BIT32);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_BIT32);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_BIT32);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_BIT32);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_BIT32);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_BIT32);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_BIT32);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_BIT32);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_BIT32);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_BIT32);
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_BIT32);

      tmsdpt[j = j + 1] = makeDynInt(0, 0, DPEL_STRUCT); //sensor0

        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_STRING); //id
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT); //idhigh
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT); //idlow
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT); //h1
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT); //h2
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT); //h3
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_FLOAT); //temp
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_BOOL); //state

      tmsdpt[j = j + 1] = makeDynInt(0, 0, DPEL_STRUCT); //sensor1

        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_STRING); //id
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT); //idhigh
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT); //idlow
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT); //h1
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT); //h2
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT); //h3
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_FLOAT); //temp
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_BOOL); //state

      tmsdpt[j = j + 1] = makeDynInt(0, 0, DPEL_STRUCT); //sensor2

        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_STRING); //id
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT); //idhigh
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT); //idlow
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT); //h1
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT); //h2
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT); //h3
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_FLOAT); //temp
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_BOOL); //state

      tmsdpt[j = j + 1] = makeDynInt(0, 0, DPEL_STRUCT); //sensor3

        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_STRING); //id
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT); //idhigh
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT); //idlow
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT); //h1
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT); //h2
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_INT); //h3
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_FLOAT); //temp
        tmsdpt[j = j + 1] = makeDynInt(0, 0, 0, DPEL_BOOL); //state

  n = dpTypeCreate(msdpt, tmsdpt);

  if(n == 0) {

    DebugN("Etsi bravo.");
    DebugN("Datapoint type created.");
  }
}

void stgBmon_createTestingDp() {

  int n;

  n = dpCreate("Test", "Bfield_Monitor");

  if(n == 0) {DebugN("Datapoint created.");}
  else {
    DebugN("Datapoint creation failed.");
  }
}

void stgBmon_deleteTestingDp() {

  int n1, n2;

  n1 = dpDelete("Test_1");
  n2 = dpDelete("Test_2");

  if(n1 == 0 && n2 == 0) {

    DebugN("Datapoints deleted.");
  }
  else {

    DebugN("oxi, oxi.");
  }

}

void stgBmon_deleteTestingDpt() {

  int n;

   n = dpTypeDelete("Bfield_Monitor");

   if(n = 0) {

     DebugN("Datapoint Type deleted");
   }
 }
