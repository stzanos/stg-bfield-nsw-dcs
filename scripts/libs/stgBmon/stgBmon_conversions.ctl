//--------------------------------------------------------------------------------
/*
  @author Stamatios Tzanos, NTUA (stzanos: stamatios.tzanos@cern.ch)

  "stgBmon_conversions.ctl" is a library to be used to convert or jump
  between useful values that depend on each other because of mapping,
  connectivity or readout.
*/

#uses "stgBmon/stgBmon_constants.ctl"

string stgBmon_stateIntToString(int state) {

  switch(state) {
    case BSENSOR_OKAY:
      return "OK";
      break;
    case BSENSOR_DEAD:
      return "DEAD";
      break;
    case BSENSOR_BAD:
      return "BAD";
      break;
    case BSENSOR_EMG:
      return "EMG";
      break;
    case BSENSOR_TEMP:
      return "TEMP";
      break;
    case BSENSOR_MASK:
      return "MASKED";
      break;
    case BSENSOR_UNK:
      return "UNK";
      break;
    case BSENSOR_NOID:
      return "NO ID";
      break;
  }
}


// ------------------------
//
//
// ------------------------
int stgBmon_sensorToSensorTemp(int sensor) {

  switch(sensor) {
    case 0:
      return 3;
      break;
    case 1:
      return 7;
      break;
    case 2:
      return 11;
      break;
    case 3:
      return 15;
      break;
  }
}

// ------------------------
//
//
// ------------------------
int stgBmon_convertUInt(int uintAdc) {

  if(uintAdc > 8388608) return uintAdc - 16777215;
  else return uintAdc;
}

// ------------------------
//
//
// ------------------------
int stgBmon_convertUIntSimulation(int uintAdc) {

  if(uintAdc > 16384) // Using random numbers. Real value: 8388608
    return uintAdc - 32767; // Using random numbers. Real value: 16777215
  else
    return uintAdc;
}



// ------------------------
//
//
// ------------------------
float stgBmon_convertHallValue(int hval) {

  float h0 = 5000; // random value
  float B0 = 0.4; // random mT

  return hval/h0 + B0;
}

// ------------------------
// Sensor numbering on sector to node number (side on spoke)
// ------------------------
int stgBmon_sensorToNodeNum(int sensor) {

  switch(sensor) {
    case 1:
      return 1;
      break;
    case 2:
      return 3;
      break;
    case 3:
      return 1;
      break;
    case 4:
      return 3;
      break;
    case 5:
      return 2;
      break;
    case 6:
      return 2;
      break;
    case 7:
      return 4;
      break;
    case 8:
      return 4;
      break;
    case 9:
      return 2;
      break;
    case 10:
      return 2;
      break;
    case 11:
      return 4;
      break;
    case 12:
      return 4;
      break;
    }
}

// ------------------------
// Sector number to effective chamber the node (side on spoke)
// is monitoring
// ------------------------
dyn_string stgBmon_sectorToEffectiveNodes(int sector) {

  switch(sector) {
    case 1:
      return makeDynString("MMG", "STG", "STG", "MMG");
      break;
    case 3:
      return makeDynString("STG", "MMG", "STG", "MMG");
      break;
    case 5:
      return makeDynString("STG", "MMG", "STG", "MMG");
      break;
    case 7:
      return makeDynString("STG", "MMG", "STG", "MMG");
      break;
    case 9:
      return makeDynString("STG", "MMG", "STG", "MMG");
      break;
    case 11:
      return makeDynString("STG", "MMG", "STG", "MMG");
      break;
    case 13:
      return makeDynString("STG", "MMG", "STG", "MMG");
      break;
    case 15:
      return makeDynString("MMG", "STG", "STG", "MMG");
      break;
  }

}

// ------------------------
// Sensor on sector to sensor input on MDM
// ------------------------
int stgBmon_sensorToSensorInput(int sensor) {

  switch(sensor) {
    case 1:
      return 0;
      break;
    case 2:
      return 0;
      break;
    case 3:
      return 1;
      break;
    case 4:
      return 1;
      break;
    case 5:
      return 2;
      break;
    case 6:
      return 0;
      break;
    case 7:
      return 0;
      break;
    case 8:
      return 2;
      break;
    case 9:
      return 3;
      break;
    case 10:
      return 1;
      break;
    case 11:
      return 1;
      break;
    case 12:
      return 3;
      break;
  }
}

// ------------------------
// Sensor on sector to sensor label
// ------------------------
string stgBmon_sensorToSensorLabel(int sensor) {

  switch(sensor) {
    case 1:
      return "1A";
      break;
    case 2:
      return "1B";
      break;
    case 3:
      return "2A";
      break;
    case 4:
      return "2B";
      break;
    case 5:
      return "3A";
      break;
    case 6:
      return "3B";
      break;
    case 7:
      return "3C";
      break;
    case 8:
      return "3D";
      break;
    case 9:
      return "4A";
      break;
    case 10:
      return "4B";
      break;
    case 11:
      return "4C";
      break;
    case 12:
      return "4D";
      break;
  }
}

// ------------------------
// Sensor label to sensor on sector
// ------------------------
int stgBmon_sensorLabelToSensor(string label) {

  switch(label) {
    case "1A":
      return 1;
      break;
    case "1B":
      return 2;
      break;
    case "2A":
      return 3;
      break;
    case "2B":
      return 4;
      break;
    case "3A":
      return 5;
      break;
    case "3B":
      return 6;
      break;
    case "3C":
      return 7;
      break;
    case "3D":
      return 8;
      break;
    case "4A":
      return 9;
      break;
    case "4B":
      return 10;
      break;
    case "4C":
      return 11;
      break;
    case "4D":
      return 12;
      break;
  }
}

// ------------------------
// Adc field (data: adc0, adc1, ... , adc15) to sensor input on MDM
// ------------------------
int stgBmon_adcFieldToSensorInput(int adc) {

  if(adc < 4)
    return 0;
  else if(adc < 8)
    return 1;
  else if(adc < 12)
    return 2;
  else
    return 3;
}

// ------------------------
// Sector number to sector string
// ------------------------
string stgBmon_sectorToSectorString(string side, int sector) {

  if(sector < 10) {
    return side + "0" + sector;
  }
  else {
    return side + sector;
  }
}

// ------------------------
// Side and sector string cut with side
// ------------------------
string stgBmon_sideSectorStringToSide(string sideSector) {

  return substr(sideSector, 0, 1);
}

// ------------------------
// Side and sector string cut with sector
// ------------------------
int stgBmon_sideSectorStringToSector(string sideSector) {

  string sector = substr(sideSector, 1, 2);
  return (int)sector;
}

// ------------------------
// Node number (side on spoke) to sensor labels
// ------------------------
dyn_string stgBmon_nodeToSensors(int node) {

  if(node == 1) {

    return makeDynString("1A", "2A", "", "");
  }
  else if(node == 2) {

    return makeDynString("3B", "4B", "3A", "4A");
  }
  else if(node == 3) {

    return makeDynString("1B", "2B", "", "");
  }
  else {

    return makeDynString("3C", "4C", "3D", "4D");
  }
}

// ------------------------
// Node number (side on spoke) to mask
// ------------------------
int stgBmon_nodeToMaskInt(int node) {

  if(node == 1 || node == 3) return 3;
  else return 16;
}

// ------------------------
// Node number (side on spoke) to sensors on sector
// ------------------------
dyn_int stgBmon_nodeToSensorNums(int node) {

  if(node == 1) {

    return makeDynString(1, 3, 0, 0);
  }
  else if(node == 2) {

    return makeDynString(6, 10, 5, 9);
  }
  else if(node == 3) {

    return makeDynString(2, 4, 0, 0);
  }
  else {

    return makeDynString(7, 11, 8, 12);
  }
}

// ------------------------
// Mask int to mask hex
// ------------------------
string stgBmon_maskIntToMaskHex(int mask) {

  switch(mask) {
    case 1:
      return "0x1";
      break;
    case 2:
      return "0x2";
      break;
    case 3:
      return "0x3";
      break;
    case 5:
      return "0x5";
      break;
    case 7:
      return "0x7";
      break;
    case 10:
      return "0xA";
      break;
    case 11:
      return "0xB";
      break;
    case 16:
      return "0xF";
      break;
  }
}

// ------------------------
// Mask int to number of sensors installed on the MDM
// ------------------------
int stgBmon_maskIntToNumberOfSensors(int mask) {

  switch(mask) {
    case 1:
      return 1;
      break;
    case 2:
      return 1;
      break;
    case 3:
      return 2;
      break;
    case 5:
      return 2;
      break;
    case 7:
      return 3;
      break;
    case 10:
      return 2;
      break;
    case 11:
      return 3;
      break;
    case 16:
      return 4;
      break;
  }
}

// ------------------------
// Mask int to active sensors on MDM
// ------------------------
dyn_bool stgBmon_maskToActiveSensorDPs(int mask) {

  switch(mask) {
    case 1:
      return makeDynBool(TRUE, FALSE, FALSE, FALSE);
      break;
    case 2:
      return makeDynBool(FALSE, TRUE, FALSE, FALSE);
      break;
    case 3:
      return makeDynBool(TRUE, TRUE, FALSE, FALSE);
      break;
    case 5:
      return makeDynBool(TRUE, FALSE, TRUE, FALSE);
      break;
    case 7:
      return makeDynBool(TRUE, TRUE, TRUE, FALSE);
      break;
    case 10:
      return makeDynBool(FALSE, TRUE, FALSE, TRUE);
      break;
    case 11:
      return makeDynBool(TRUE, TRUE, FALSE, TRUE);
      break;
    case 16:
      return makeDynBool(TRUE, TRUE, TRUE, TRUE);
      break;
  }
}
