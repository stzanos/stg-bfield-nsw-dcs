//--------------------------------------------------------------------------------
/*
  @author Stamatios Tzanos, NTUA (stzanos: stamatios.tzanos@cern.ch)

  "stgBmon_infrastructure.ctl" is a library of infrastructure actions and datapoint
  manipulations for the project to function properly."
*/

#uses "stgBmon/stgBmon_constants.ctl"

// ------------------------
// Return the states of all sensors connected on a sector
// ------------------------
dyn_string stgBmon_getSectorSensorStates(string side, int sector) {

  string sys = stgBmon_returnProjectName(), state;
  string dp = sys + "EIZ4" + side + sector;
  dyn_string states;
  int i = 1;

  for(int n = 1; n <= 4; n++) {
    for(int s = 0; s < 4; s++) {
      dpGet(dp + ".node" + n + ".bf.sensor" + s + ".state", state);
      states[i] = state;
      i++;
    }
  }

  return states;
}
// ------------------------
// Return the number of OK states
// ------------------------
int stgBmon_getNumberOfOkSensors(string side, int sector) {

  dyn_string states = stgBmon_getSectorSensorStates(side, sector);
  int oks = 0;

  for(int i = 1; i <= dynlen(states); i++) {
    if(states[i] == "OK")
      oks++;
  }

  return oks;
}
// ------------------------
// Return the number of WARNING states
// ------------------------
int stgBmon_getNumberOfWarningSensors(string side, int sector) {

  dyn_string states = stgBmon_getSectorSensorStates(side, sector);
  int warnings = 0;

  for(int i = 1; i <= dynlen(states); i++) {
    if(states[i] == "WARNING")
      warnings++;
  }

  return warnings;
}
// ------------------------
// Return the number of ERROR states
// ------------------------
int stgBmon_getNumberOfErrorSensors(string side, int sector) {

  dyn_string states = stgBmon_getSectorSensorStates(side, sector);
  int errors = 0;

  for(int i = 1; i <= dynlen(states); i++) {
    if(states[i] == "FATAL")
      errors++;
  }

  return errors;
}
// ------------------------
// Return the number of FATAL states
// ------------------------
int stgBmon_getNumberOfFatalSensors(string side, int sector) {

  dyn_string states = stgBmon_getSectorSensorStates(side, sector);
  int fatals = 0;

  for(int i = 1; i <= dynlen(states); i++) {
    if(states[i] == "FATAL")
      fatals++;
  }

  return fatals;
}

void stgBmon_createDptSector() {

  int i, j;
  dyn_dyn_string msdpt;
  dyn_dyn_int tmsdpt;

  int n;
  dyn_errClass err;

  i=1;
  j=1;

  // Create the data type


  msdpt[i] = makeDynString ("NSWMDM_BField","","","","");

    msdpt[i=i+1] = makeDynString ("","BcalAverage", "","");

    msdpt[i=i+1] = makeDynString ("","node1","","","");

      msdpt[i=i+1] = makeDynString ("","","id","","");
      msdpt[i=i+1] = makeDynString ("","","canbus","","");
      msdpt[i=i+1] = makeDynString ("","","canport","","");
      msdpt[i=i+1] = makeDynString ("","","busid","","");
      msdpt[i=i+1] = makeDynString ("","","state","","");

      msdpt[i=i+1] = makeDynString ("","","serial","","");

        msdpt[i=i+1] = makeDynString ("","","","serialnr","");

      msdpt[i=i+1] = makeDynString ("","","bf","","");

        msdpt[i=i+1] = makeDynString ("","","","bfMask","");
        msdpt[i=i+1] = makeDynString ("","","","state","");

        msdpt[i=i+1] = makeDynString ("","","","data","");

          msdpt[i=i+1] = makeDynString ("","","","","adc0");
          msdpt[i=i+1] = makeDynString ("","","","","adc1");
          msdpt[i=i+1] = makeDynString ("","","","","adc2");
          msdpt[i=i+1] = makeDynString ("","","","","adc3");
          msdpt[i=i+1] = makeDynString ("","","","","adc4");
          msdpt[i=i+1] = makeDynString ("","","","","adc5");
          msdpt[i=i+1] = makeDynString ("","","","","adc6");
          msdpt[i=i+1] = makeDynString ("","","","","adc7");
          msdpt[i=i+1] = makeDynString ("","","","","adc8");
          msdpt[i=i+1] = makeDynString ("","","","","adc9");
          msdpt[i=i+1] = makeDynString ("","","","","adc10");
          msdpt[i=i+1] = makeDynString ("","","","","adc11");
          msdpt[i=i+1] = makeDynString ("","","","","adc12");
          msdpt[i=i+1] = makeDynString ("","","","","adc13");
          msdpt[i=i+1] = makeDynString ("","","","","adc14");
          msdpt[i=i+1] = makeDynString ("","","","","adc15");

          msdpt[i=i+1] = makeDynString ("","","","","conf0");
          msdpt[i=i+1] = makeDynString ("","","","","conf1");
          msdpt[i=i+1] = makeDynString ("","","","","conf2");
          msdpt[i=i+1] = makeDynString ("","","","","conf3");
          msdpt[i=i+1] = makeDynString ("","","","","conf4");
          msdpt[i=i+1] = makeDynString ("","","","","conf5");
          msdpt[i=i+1] = makeDynString ("","","","","conf6");
          msdpt[i=i+1] = makeDynString ("","","","","conf7");
          msdpt[i=i+1] = makeDynString ("","","","","conf8");
          msdpt[i=i+1] = makeDynString ("","","","","conf9");
          msdpt[i=i+1] = makeDynString ("","","","","conf10");
          msdpt[i=i+1] = makeDynString ("","","","","conf11");
          msdpt[i=i+1] = makeDynString ("","","","","conf12");
          msdpt[i=i+1] = makeDynString ("","","","","conf13");
          msdpt[i=i+1] = makeDynString ("","","","","conf14");
          msdpt[i=i+1] = makeDynString ("","","","","conf15");

        msdpt[i=i+1] = makeDynString ("","","","sensor0","");

          msdpt[i=i+1] = makeDynString ("","","","","label");
          msdpt[i=i+1] = makeDynString ("","","","","name");
          msdpt[i=i+1] = makeDynString ("","","","","id");
          msdpt[i=i+1] = makeDynString ("","","","","idhigh");
          msdpt[i=i+1] = makeDynString ("","","","","idlow");
          msdpt[i=i+1] = makeDynString ("","","","","h1");
          msdpt[i=i+1] = makeDynString ("","","","","h2");
          msdpt[i=i+1] = makeDynString ("","","","","h3");
          msdpt[i=i+1] = makeDynString ("","","","","temp");
          msdpt[i=i+1] = makeDynString ("","","","","Bx");
          msdpt[i=i+1] = makeDynString ("","","","","By");
          msdpt[i=i+1] = makeDynString ("","","","","Bz");
          msdpt[i=i+1] = makeDynString ("","","","","Bcal");
          msdpt[i=i+1] = makeDynString ("","","","","state");

        msdpt[i=i+1] = makeDynString ("","","","sensor1","");

          msdpt[i=i+1] = makeDynString ("","","","","label");
          msdpt[i=i+1] = makeDynString ("","","","","name");
          msdpt[i=i+1] = makeDynString ("","","","","id");
          msdpt[i=i+1] = makeDynString ("","","","","idhigh");
          msdpt[i=i+1] = makeDynString ("","","","","idlow");
          msdpt[i=i+1] = makeDynString ("","","","","h1");
          msdpt[i=i+1] = makeDynString ("","","","","h2");
          msdpt[i=i+1] = makeDynString ("","","","","h3");
          msdpt[i=i+1] = makeDynString ("","","","","temp");
          msdpt[i=i+1] = makeDynString ("","","","","Bx");
          msdpt[i=i+1] = makeDynString ("","","","","By");
          msdpt[i=i+1] = makeDynString ("","","","","Bz");
          msdpt[i=i+1] = makeDynString ("","","","","Bcal");
          msdpt[i=i+1] = makeDynString ("","","","","state");

        msdpt[i=i+1] = makeDynString ("","","","sensor2","");

          msdpt[i=i+1] = makeDynString ("","","","","label");
          msdpt[i=i+1] = makeDynString ("","","","","name");
          msdpt[i=i+1] = makeDynString ("","","","","id");
          msdpt[i=i+1] = makeDynString ("","","","","idhigh");
          msdpt[i=i+1] = makeDynString ("","","","","idlow");
          msdpt[i=i+1] = makeDynString ("","","","","h1");
          msdpt[i=i+1] = makeDynString ("","","","","h2");
          msdpt[i=i+1] = makeDynString ("","","","","h3");
          msdpt[i=i+1] = makeDynString ("","","","","temp");
          msdpt[i=i+1] = makeDynString ("","","","","Bx");
          msdpt[i=i+1] = makeDynString ("","","","","By");
          msdpt[i=i+1] = makeDynString ("","","","","Bz");
          msdpt[i=i+1] = makeDynString ("","","","","Bcal");
          msdpt[i=i+1] = makeDynString ("","","","","state");

        msdpt[i=i+1] = makeDynString ("","","","sensor3","");

          msdpt[i=i+1] = makeDynString ("","","","","label");
          msdpt[i=i+1] = makeDynString ("","","","","name");
          msdpt[i=i+1] = makeDynString ("","","","","id");
          msdpt[i=i+1] = makeDynString ("","","","","idhigh");
          msdpt[i=i+1] = makeDynString ("","","","","idlow");
          msdpt[i=i+1] = makeDynString ("","","","","h1");
          msdpt[i=i+1] = makeDynString ("","","","","h2");
          msdpt[i=i+1] = makeDynString ("","","","","h3");
          msdpt[i=i+1] = makeDynString ("","","","","temp");
          msdpt[i=i+1] = makeDynString ("","","","","Bx");
          msdpt[i=i+1] = makeDynString ("","","","","By");
          msdpt[i=i+1] = makeDynString ("","","","","Bz");
          msdpt[i=i+1] = makeDynString ("","","","","Bcal");
          msdpt[i=i+1] = makeDynString ("","","","","state");

    msdpt[i=i+1] = makeDynString ("","node2","","","");

      msdpt[i=i+1] = makeDynString ("","","id","","");
      msdpt[i=i+1] = makeDynString ("","","canbus","","");
      msdpt[i=i+1] = makeDynString ("","","canport","","");
      msdpt[i=i+1] = makeDynString ("","","busid","","");
      msdpt[i=i+1] = makeDynString ("","","state","","");

      msdpt[i=i+1] = makeDynString ("","","serial","","");

        msdpt[i=i+1] = makeDynString ("","","","serialnr","");

      msdpt[i=i+1] = makeDynString ("","","bf","","");

        msdpt[i=i+1] = makeDynString ("","","","bfMask","");
        msdpt[i=i+1] = makeDynString ("","","","state","");

        msdpt[i=i+1] = makeDynString ("","","","data","");

          msdpt[i=i+1] = makeDynString ("","","","","adc0");
          msdpt[i=i+1] = makeDynString ("","","","","adc1");
          msdpt[i=i+1] = makeDynString ("","","","","adc2");
          msdpt[i=i+1] = makeDynString ("","","","","adc3");
          msdpt[i=i+1] = makeDynString ("","","","","adc4");
          msdpt[i=i+1] = makeDynString ("","","","","adc5");
          msdpt[i=i+1] = makeDynString ("","","","","adc6");
          msdpt[i=i+1] = makeDynString ("","","","","adc7");
          msdpt[i=i+1] = makeDynString ("","","","","adc8");
          msdpt[i=i+1] = makeDynString ("","","","","adc9");
          msdpt[i=i+1] = makeDynString ("","","","","adc10");
          msdpt[i=i+1] = makeDynString ("","","","","adc11");
          msdpt[i=i+1] = makeDynString ("","","","","adc12");
          msdpt[i=i+1] = makeDynString ("","","","","adc13");
          msdpt[i=i+1] = makeDynString ("","","","","adc14");
          msdpt[i=i+1] = makeDynString ("","","","","adc15");

          msdpt[i=i+1] = makeDynString ("","","","","conf0");
          msdpt[i=i+1] = makeDynString ("","","","","conf1");
          msdpt[i=i+1] = makeDynString ("","","","","conf2");
          msdpt[i=i+1] = makeDynString ("","","","","conf3");
          msdpt[i=i+1] = makeDynString ("","","","","conf4");
          msdpt[i=i+1] = makeDynString ("","","","","conf5");
          msdpt[i=i+1] = makeDynString ("","","","","conf6");
          msdpt[i=i+1] = makeDynString ("","","","","conf7");
          msdpt[i=i+1] = makeDynString ("","","","","conf8");
          msdpt[i=i+1] = makeDynString ("","","","","conf9");
          msdpt[i=i+1] = makeDynString ("","","","","conf10");
          msdpt[i=i+1] = makeDynString ("","","","","conf11");
          msdpt[i=i+1] = makeDynString ("","","","","conf12");
          msdpt[i=i+1] = makeDynString ("","","","","conf13");
          msdpt[i=i+1] = makeDynString ("","","","","conf14");
          msdpt[i=i+1] = makeDynString ("","","","","conf15");

        msdpt[i=i+1] = makeDynString ("","","","sensor0","");

          msdpt[i=i+1] = makeDynString ("","","","","label");
          msdpt[i=i+1] = makeDynString ("","","","","name");
          msdpt[i=i+1] = makeDynString ("","","","","id");
          msdpt[i=i+1] = makeDynString ("","","","","idhigh");
          msdpt[i=i+1] = makeDynString ("","","","","idlow");
          msdpt[i=i+1] = makeDynString ("","","","","h1");
          msdpt[i=i+1] = makeDynString ("","","","","h2");
          msdpt[i=i+1] = makeDynString ("","","","","h3");
          msdpt[i=i+1] = makeDynString ("","","","","temp");
          msdpt[i=i+1] = makeDynString ("","","","","Bx");
          msdpt[i=i+1] = makeDynString ("","","","","By");
          msdpt[i=i+1] = makeDynString ("","","","","Bz");
          msdpt[i=i+1] = makeDynString ("","","","","Bcal");
          msdpt[i=i+1] = makeDynString ("","","","","state");

        msdpt[i=i+1] = makeDynString ("","","","sensor1","");

          msdpt[i=i+1] = makeDynString ("","","","","label");
          msdpt[i=i+1] = makeDynString ("","","","","name");
          msdpt[i=i+1] = makeDynString ("","","","","id");
          msdpt[i=i+1] = makeDynString ("","","","","idhigh");
          msdpt[i=i+1] = makeDynString ("","","","","idlow");
          msdpt[i=i+1] = makeDynString ("","","","","h1");
          msdpt[i=i+1] = makeDynString ("","","","","h2");
          msdpt[i=i+1] = makeDynString ("","","","","h3");
          msdpt[i=i+1] = makeDynString ("","","","","temp");
          msdpt[i=i+1] = makeDynString ("","","","","Bx");
          msdpt[i=i+1] = makeDynString ("","","","","By");
          msdpt[i=i+1] = makeDynString ("","","","","Bz");
          msdpt[i=i+1] = makeDynString ("","","","","Bcal");
          msdpt[i=i+1] = makeDynString ("","","","","state");

        msdpt[i=i+1] = makeDynString ("","","","sensor2","");

          msdpt[i=i+1] = makeDynString ("","","","","label");
          msdpt[i=i+1] = makeDynString ("","","","","name");
          msdpt[i=i+1] = makeDynString ("","","","","id");
          msdpt[i=i+1] = makeDynString ("","","","","idhigh");
          msdpt[i=i+1] = makeDynString ("","","","","idlow");
          msdpt[i=i+1] = makeDynString ("","","","","h1");
          msdpt[i=i+1] = makeDynString ("","","","","h2");
          msdpt[i=i+1] = makeDynString ("","","","","h3");
          msdpt[i=i+1] = makeDynString ("","","","","temp");
          msdpt[i=i+1] = makeDynString ("","","","","Bx");
          msdpt[i=i+1] = makeDynString ("","","","","By");
          msdpt[i=i+1] = makeDynString ("","","","","Bz");
          msdpt[i=i+1] = makeDynString ("","","","","Bcal");
          msdpt[i=i+1] = makeDynString ("","","","","state");

        msdpt[i=i+1] = makeDynString ("","","","sensor3","");

          msdpt[i=i+1] = makeDynString ("","","","","label");
          msdpt[i=i+1] = makeDynString ("","","","","name");
          msdpt[i=i+1] = makeDynString ("","","","","id");
          msdpt[i=i+1] = makeDynString ("","","","","idhigh");
          msdpt[i=i+1] = makeDynString ("","","","","idlow");
          msdpt[i=i+1] = makeDynString ("","","","","h1");
          msdpt[i=i+1] = makeDynString ("","","","","h2");
          msdpt[i=i+1] = makeDynString ("","","","","h3");
          msdpt[i=i+1] = makeDynString ("","","","","temp");
          msdpt[i=i+1] = makeDynString ("","","","","Bx");
          msdpt[i=i+1] = makeDynString ("","","","","By");
          msdpt[i=i+1] = makeDynString ("","","","","Bz");
          msdpt[i=i+1] = makeDynString ("","","","","Bcal");
          msdpt[i=i+1] = makeDynString ("","","","","state");

    msdpt[i=i+1] = makeDynString ("","node3","","","");

      msdpt[i=i+1] = makeDynString ("","","id","","");
      msdpt[i=i+1] = makeDynString ("","","canbus","","");
      msdpt[i=i+1] = makeDynString ("","","canport","","");
      msdpt[i=i+1] = makeDynString ("","","busid","","");
      msdpt[i=i+1] = makeDynString ("","","state","","");

      msdpt[i=i+1] = makeDynString ("","","serial","","");

        msdpt[i=i+1] = makeDynString ("","","","serialnr","");

      msdpt[i=i+1] = makeDynString ("","","bf","","");

        msdpt[i=i+1] = makeDynString ("","","","bfMask","");
        msdpt[i=i+1] = makeDynString ("","","","state","");

        msdpt[i=i+1] = makeDynString ("","","","data","");

          msdpt[i=i+1] = makeDynString ("","","","","adc0");
          msdpt[i=i+1] = makeDynString ("","","","","adc1");
          msdpt[i=i+1] = makeDynString ("","","","","adc2");
          msdpt[i=i+1] = makeDynString ("","","","","adc3");
          msdpt[i=i+1] = makeDynString ("","","","","adc4");
          msdpt[i=i+1] = makeDynString ("","","","","adc5");
          msdpt[i=i+1] = makeDynString ("","","","","adc6");
          msdpt[i=i+1] = makeDynString ("","","","","adc7");
          msdpt[i=i+1] = makeDynString ("","","","","adc8");
          msdpt[i=i+1] = makeDynString ("","","","","adc9");
          msdpt[i=i+1] = makeDynString ("","","","","adc10");
          msdpt[i=i+1] = makeDynString ("","","","","adc11");
          msdpt[i=i+1] = makeDynString ("","","","","adc12");
          msdpt[i=i+1] = makeDynString ("","","","","adc13");
          msdpt[i=i+1] = makeDynString ("","","","","adc14");
          msdpt[i=i+1] = makeDynString ("","","","","adc15");

          msdpt[i=i+1] = makeDynString ("","","","","conf0");
          msdpt[i=i+1] = makeDynString ("","","","","conf1");
          msdpt[i=i+1] = makeDynString ("","","","","conf2");
          msdpt[i=i+1] = makeDynString ("","","","","conf3");
          msdpt[i=i+1] = makeDynString ("","","","","conf4");
          msdpt[i=i+1] = makeDynString ("","","","","conf5");
          msdpt[i=i+1] = makeDynString ("","","","","conf6");
          msdpt[i=i+1] = makeDynString ("","","","","conf7");
          msdpt[i=i+1] = makeDynString ("","","","","conf8");
          msdpt[i=i+1] = makeDynString ("","","","","conf9");
          msdpt[i=i+1] = makeDynString ("","","","","conf10");
          msdpt[i=i+1] = makeDynString ("","","","","conf11");
          msdpt[i=i+1] = makeDynString ("","","","","conf12");
          msdpt[i=i+1] = makeDynString ("","","","","conf13");
          msdpt[i=i+1] = makeDynString ("","","","","conf14");
          msdpt[i=i+1] = makeDynString ("","","","","conf15");

        msdpt[i=i+1] = makeDynString ("","","","sensor0","");

          msdpt[i=i+1] = makeDynString ("","","","","label");
          msdpt[i=i+1] = makeDynString ("","","","","name");
          msdpt[i=i+1] = makeDynString ("","","","","id");
          msdpt[i=i+1] = makeDynString ("","","","","idhigh");
          msdpt[i=i+1] = makeDynString ("","","","","idlow");
          msdpt[i=i+1] = makeDynString ("","","","","h1");
          msdpt[i=i+1] = makeDynString ("","","","","h2");
          msdpt[i=i+1] = makeDynString ("","","","","h3");
          msdpt[i=i+1] = makeDynString ("","","","","temp");
          msdpt[i=i+1] = makeDynString ("","","","","Bx");
          msdpt[i=i+1] = makeDynString ("","","","","By");
          msdpt[i=i+1] = makeDynString ("","","","","Bz");
          msdpt[i=i+1] = makeDynString ("","","","","Bcal");
          msdpt[i=i+1] = makeDynString ("","","","","state");

        msdpt[i=i+1] = makeDynString ("","","","sensor1","");

          msdpt[i=i+1] = makeDynString ("","","","","label");
          msdpt[i=i+1] = makeDynString ("","","","","name");
          msdpt[i=i+1] = makeDynString ("","","","","id");
          msdpt[i=i+1] = makeDynString ("","","","","idhigh");
          msdpt[i=i+1] = makeDynString ("","","","","idlow");
          msdpt[i=i+1] = makeDynString ("","","","","h1");
          msdpt[i=i+1] = makeDynString ("","","","","h2");
          msdpt[i=i+1] = makeDynString ("","","","","h3");
          msdpt[i=i+1] = makeDynString ("","","","","temp");
          msdpt[i=i+1] = makeDynString ("","","","","Bx");
          msdpt[i=i+1] = makeDynString ("","","","","By");
          msdpt[i=i+1] = makeDynString ("","","","","Bz");
          msdpt[i=i+1] = makeDynString ("","","","","Bcal");
          msdpt[i=i+1] = makeDynString ("","","","","state");

        msdpt[i=i+1] = makeDynString ("","","","sensor2","");

          msdpt[i=i+1] = makeDynString ("","","","","label");
          msdpt[i=i+1] = makeDynString ("","","","","name");
          msdpt[i=i+1] = makeDynString ("","","","","id");
          msdpt[i=i+1] = makeDynString ("","","","","idhigh");
          msdpt[i=i+1] = makeDynString ("","","","","idlow");
          msdpt[i=i+1] = makeDynString ("","","","","h1");
          msdpt[i=i+1] = makeDynString ("","","","","h2");
          msdpt[i=i+1] = makeDynString ("","","","","h3");
          msdpt[i=i+1] = makeDynString ("","","","","temp");
          msdpt[i=i+1] = makeDynString ("","","","","Bx");
          msdpt[i=i+1] = makeDynString ("","","","","By");
          msdpt[i=i+1] = makeDynString ("","","","","Bz");
          msdpt[i=i+1] = makeDynString ("","","","","Bcal");
          msdpt[i=i+1] = makeDynString ("","","","","state");

        msdpt[i=i+1] = makeDynString ("","","","sensor3","");

          msdpt[i=i+1] = makeDynString ("","","","","label");
          msdpt[i=i+1] = makeDynString ("","","","","name");
          msdpt[i=i+1] = makeDynString ("","","","","id");
          msdpt[i=i+1] = makeDynString ("","","","","idhigh");
          msdpt[i=i+1] = makeDynString ("","","","","idlow");
          msdpt[i=i+1] = makeDynString ("","","","","h1");
          msdpt[i=i+1] = makeDynString ("","","","","h2");
          msdpt[i=i+1] = makeDynString ("","","","","h3");
          msdpt[i=i+1] = makeDynString ("","","","","temp");
          msdpt[i=i+1] = makeDynString ("","","","","Bx");
          msdpt[i=i+1] = makeDynString ("","","","","By");
          msdpt[i=i+1] = makeDynString ("","","","","Bz");
          msdpt[i=i+1] = makeDynString ("","","","","Bcal");
          msdpt[i=i+1] = makeDynString ("","","","","state");

    msdpt[i=i+1] = makeDynString ("","node4","","","");

      msdpt[i=i+1] = makeDynString ("","","id","","");
      msdpt[i=i+1] = makeDynString ("","","canbus","","");
      msdpt[i=i+1] = makeDynString ("","","canport","","");
      msdpt[i=i+1] = makeDynString ("","","busid","","");
      msdpt[i=i+1] = makeDynString ("","","state","","");

      msdpt[i=i+1] = makeDynString ("","","serial","","");

        msdpt[i=i+1] = makeDynString ("","","","serialnr","");

      msdpt[i=i+1] = makeDynString ("","","bf","","");

        msdpt[i=i+1] = makeDynString ("","","","bfMask","");
        msdpt[i=i+1] = makeDynString ("","","","state","");

        msdpt[i=i+1] = makeDynString ("","","","data","");

          msdpt[i=i+1] = makeDynString ("","","","","adc0");
          msdpt[i=i+1] = makeDynString ("","","","","adc1");
          msdpt[i=i+1] = makeDynString ("","","","","adc2");
          msdpt[i=i+1] = makeDynString ("","","","","adc3");
          msdpt[i=i+1] = makeDynString ("","","","","adc4");
          msdpt[i=i+1] = makeDynString ("","","","","adc5");
          msdpt[i=i+1] = makeDynString ("","","","","adc6");
          msdpt[i=i+1] = makeDynString ("","","","","adc7");
          msdpt[i=i+1] = makeDynString ("","","","","adc8");
          msdpt[i=i+1] = makeDynString ("","","","","adc9");
          msdpt[i=i+1] = makeDynString ("","","","","adc10");
          msdpt[i=i+1] = makeDynString ("","","","","adc11");
          msdpt[i=i+1] = makeDynString ("","","","","adc12");
          msdpt[i=i+1] = makeDynString ("","","","","adc13");
          msdpt[i=i+1] = makeDynString ("","","","","adc14");
          msdpt[i=i+1] = makeDynString ("","","","","adc15");

          msdpt[i=i+1] = makeDynString ("","","","","conf0");
          msdpt[i=i+1] = makeDynString ("","","","","conf1");
          msdpt[i=i+1] = makeDynString ("","","","","conf2");
          msdpt[i=i+1] = makeDynString ("","","","","conf3");
          msdpt[i=i+1] = makeDynString ("","","","","conf4");
          msdpt[i=i+1] = makeDynString ("","","","","conf5");
          msdpt[i=i+1] = makeDynString ("","","","","conf6");
          msdpt[i=i+1] = makeDynString ("","","","","conf7");
          msdpt[i=i+1] = makeDynString ("","","","","conf8");
          msdpt[i=i+1] = makeDynString ("","","","","conf9");
          msdpt[i=i+1] = makeDynString ("","","","","conf10");
          msdpt[i=i+1] = makeDynString ("","","","","conf11");
          msdpt[i=i+1] = makeDynString ("","","","","conf12");
          msdpt[i=i+1] = makeDynString ("","","","","conf13");
          msdpt[i=i+1] = makeDynString ("","","","","conf14");
          msdpt[i=i+1] = makeDynString ("","","","","conf15");

        msdpt[i=i+1] = makeDynString ("","","","sensor0","");

          msdpt[i=i+1] = makeDynString ("","","","","label");
          msdpt[i=i+1] = makeDynString ("","","","","name");
          msdpt[i=i+1] = makeDynString ("","","","","id");
          msdpt[i=i+1] = makeDynString ("","","","","idhigh");
          msdpt[i=i+1] = makeDynString ("","","","","idlow");
          msdpt[i=i+1] = makeDynString ("","","","","h1");
          msdpt[i=i+1] = makeDynString ("","","","","h2");
          msdpt[i=i+1] = makeDynString ("","","","","h3");
          msdpt[i=i+1] = makeDynString ("","","","","temp");
          msdpt[i=i+1] = makeDynString ("","","","","Bx");
          msdpt[i=i+1] = makeDynString ("","","","","By");
          msdpt[i=i+1] = makeDynString ("","","","","Bz");
          msdpt[i=i+1] = makeDynString ("","","","","Bcal");
          msdpt[i=i+1] = makeDynString ("","","","","state");

        msdpt[i=i+1] = makeDynString ("","","","sensor1","");

          msdpt[i=i+1] = makeDynString ("","","","","label");
          msdpt[i=i+1] = makeDynString ("","","","","name");
          msdpt[i=i+1] = makeDynString ("","","","","id");
          msdpt[i=i+1] = makeDynString ("","","","","idhigh");
          msdpt[i=i+1] = makeDynString ("","","","","idlow");
          msdpt[i=i+1] = makeDynString ("","","","","h1");
          msdpt[i=i+1] = makeDynString ("","","","","h2");
          msdpt[i=i+1] = makeDynString ("","","","","h3");
          msdpt[i=i+1] = makeDynString ("","","","","temp");
          msdpt[i=i+1] = makeDynString ("","","","","Bx");
          msdpt[i=i+1] = makeDynString ("","","","","By");
          msdpt[i=i+1] = makeDynString ("","","","","Bz");
          msdpt[i=i+1] = makeDynString ("","","","","Bcal");
          msdpt[i=i+1] = makeDynString ("","","","","state");

        msdpt[i=i+1] = makeDynString ("","","","sensor2","");

          msdpt[i=i+1] = makeDynString ("","","","","label");
          msdpt[i=i+1] = makeDynString ("","","","","name");
          msdpt[i=i+1] = makeDynString ("","","","","id");
          msdpt[i=i+1] = makeDynString ("","","","","idhigh");
          msdpt[i=i+1] = makeDynString ("","","","","idlow");
          msdpt[i=i+1] = makeDynString ("","","","","h1");
          msdpt[i=i+1] = makeDynString ("","","","","h2");
          msdpt[i=i+1] = makeDynString ("","","","","h3");
          msdpt[i=i+1] = makeDynString ("","","","","temp");
          msdpt[i=i+1] = makeDynString ("","","","","Bx");
          msdpt[i=i+1] = makeDynString ("","","","","By");
          msdpt[i=i+1] = makeDynString ("","","","","Bz");
          msdpt[i=i+1] = makeDynString ("","","","","Bcal");
          msdpt[i=i+1] = makeDynString ("","","","","state");

        msdpt[i=i+1] = makeDynString ("","","","sensor3","");

          msdpt[i=i+1] = makeDynString ("","","","","label");
          msdpt[i=i+1] = makeDynString ("","","","","name");
          msdpt[i=i+1] = makeDynString ("","","","","id");
          msdpt[i=i+1] = makeDynString ("","","","","idhigh");
          msdpt[i=i+1] = makeDynString ("","","","","idlow");
          msdpt[i=i+1] = makeDynString ("","","","","h1");
          msdpt[i=i+1] = makeDynString ("","","","","h2");
          msdpt[i=i+1] = makeDynString ("","","","","h3");
          msdpt[i=i+1] = makeDynString ("","","","","temp");
          msdpt[i=i+1] = makeDynString ("","","","","Bx");
          msdpt[i=i+1] = makeDynString ("","","","","By");
          msdpt[i=i+1] = makeDynString ("","","","","Bz");
          msdpt[i=i+1] = makeDynString ("","","","","Bcal");
          msdpt[i=i+1] = makeDynString ("","","","","state");

  //****************************************************//

  tmsdpt[j] = makeDynInt (DPEL_STRUCT);

    tmsdpt[j=j+1] = makeDynInt (0,DPEL_FLOAT);

    tmsdpt[j=j+1] = makeDynInt (0,DPEL_STRUCT); //node1

      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_INT);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_INT);

      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRUCT); //serial

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRING);

      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRUCT); //bf

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //data

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT); //adc
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32); //conf
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //sensor0

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //label
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //name
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //id
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idh
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idl
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h1
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h2
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h3
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //temp
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bx
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //By
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bz
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bcal
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //state

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //sensor1

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //label
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //name
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //id
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idh
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idl
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h1
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h2
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h3
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //temp
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bx
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //By
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bz
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bcal
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //state

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //sensor2

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //label
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //name
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //id
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idh
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idl
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h1
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h2
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h3
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //temp
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bx
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //By
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bz
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bcal
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //state

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //sensor3

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //label
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //name
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //id
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idh
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idl
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h1
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h2
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h3
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //temp
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bx
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //By
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bz
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bcal
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //state

    tmsdpt[j=j+1] = makeDynInt (0,DPEL_STRUCT); //node1

      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_INT);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_INT);

      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRUCT); //serial

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRING);

      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRUCT); //bf

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //data

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT); //adc
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32); //conf
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //sensor0

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //label
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //name
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //id
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idh
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idl
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h1
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h2
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h3
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //temp
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bx
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //By
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bz
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bcal
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //state

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //sensor1

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //label
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //name
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //id
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idh
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idl
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h1
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h2
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h3
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //temp
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bx
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //By
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bz
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bcal
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //state

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //sensor2

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //label
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //name
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //id
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idh
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idl
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h1
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h2
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h3
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //temp
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bx
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //By
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bz
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bcal
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //state

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //sensor3

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //label
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //name
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //id
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idh
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idl
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h1
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h2
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h3
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //temp
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bx
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //By
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bz
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bcal
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //state

    tmsdpt[j=j+1] = makeDynInt (0,DPEL_STRUCT); //node3

      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_INT);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_INT);

      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRUCT); //serial

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRING);

      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRUCT); //bf

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //data

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT); //adc
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32); //conf
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //sensor0

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //label
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //name
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //id
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idh
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idl
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h1
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h2
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h3
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //temp
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bx
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //By
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bz
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bcal
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //state

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //sensor1

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //label
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //name
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //id
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idh
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idl
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h1
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h2
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h3
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //temp
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bx
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //By
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bz
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bcal
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //state

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //sensor2

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //label
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //name
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //id
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idh
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idl
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h1
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h2
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h3
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //temp
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bx
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //By
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bz
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bcal
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //state

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //sensor3

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //label
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //name
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //id
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idh
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idl
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h1
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h2
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h3
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //temp
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bx
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //By
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bz
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bcal
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //state

    tmsdpt[j=j+1] = makeDynInt (0,DPEL_STRUCT); //node4

      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_INT);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_INT);

      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRUCT); //serial

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRING);

      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRUCT); //bf

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //data

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT); //adc
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32); //conf
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //sensor0

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //label
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //name
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //id
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idh
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idl
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h1
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h2
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h3
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //temp
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bx
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //By
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bz
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bcal
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //state

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //sensor1

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //label
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //name
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //id
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idh
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idl
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h1
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h2
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h3
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //temp
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bx
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //By
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bz
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bcal
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //state

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //sensor2

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //label
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //name
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //id
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idh
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idl
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h1
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h2
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h3
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //temp
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bx
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //By
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bz
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bcal
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //state

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //sensor3

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //label
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //name
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //id
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idh
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idl
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h1
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h2
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h3
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //temp
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bx
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //By
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bz
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bcal
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //state


  //************************************************//

  n = dpTypeCreate(msdpt,tmsdpt);

  DebugN ("NSWMDMChamber datapoint type created, result: ",n);

  if(n == 0)
    DebugN("Etsi bravo");

  err = getLastError();
  if(dynlen(err) > 0) {
    errorDialog(err);
    throwError(err);
  }
  else
    DebugN("OK");
}

void stgBmon_createDptInfrastructure() {

  int i, j;
  dyn_dyn_string msdpt;
  dyn_dyn_int tmsdpt;

  int n;
  dyn_errClass err;

  i = 1;
  j = 1;

  msdpt[i] = makeDynString("NSWMDM_BField_infrastructure", "");

    msdpt[i=1+1] = makeDynString("", "view");

  tmsdpt[j] = makeDynInt(DPEL_STRUCT);

    tmsdpt[j=j+1] = makeDynInt(0, DPEL_STRING);

  n = dpTypeCreate(msdpt, tmsdpt);
  DebugN("NSWMDM_BField_infrastructure datapoint type created, result: ", n);

  if(n == 0) DebugN("Etsi bravo");

  err = getLastError();
  if(dynlen(err) > 0) {
    errorDialog(err);
    throwError(err);
  }
  else
    DebugN("OK");
}

void stgBmon_createDptSimulation() {

  int i, j;
  dyn_dyn_string msdpt;
  dyn_dyn_int tmsdpt;

  int n;
  dyn_errClass err;

  i=1;
  j=1;

  // Create the data type


  msdpt[i] = makeDynString ("NSWMDMSimulation","","","","");

    msdpt[i=i+1] = makeDynString ("","name","","");
    msdpt[i=i+1] = makeDynString ("","BcalAverage", "","");
    msdpt[i=i+1] = makeDynString ("","state","","");

    msdpt[i=i+1] = makeDynString ("","node","","","");

      msdpt[i=i+1] = makeDynString ("","","id","","");
      msdpt[i=i+1] = makeDynString ("","","type","","");
      msdpt[i=i+1] = makeDynString ("","","effective","","");
      msdpt[i=i+1] = makeDynString ("","","canbus","","");
      msdpt[i=i+1] = makeDynString ("","","canport","","");
      msdpt[i=i+1] = makeDynString ("","","portErr","","");
      msdpt[i=i+1] = makeDynString ("","","busid","","");
      msdpt[i=i+1] = makeDynString ("","","state","","");

      msdpt[i=i+1] = makeDynString ("","","serial","","");

        msdpt[i=i+1] = makeDynString ("","","","serialnr","");

      msdpt[i=i+1] = makeDynString ("","","bf","","");

        msdpt[i=i+1] = makeDynString ("","","","bfMask","");
        msdpt[i=i+1] = makeDynString ("","","","errMaskc","");
        msdpt[i=i+1] = makeDynString ("","","","numberOfBsensors","");
        msdpt[i=i+1] = makeDynString ("","","","state","");

        msdpt[i=i+1] = makeDynString ("","","","data","");

          msdpt[i=i+1] = makeDynString ("","","","","adc0");
          msdpt[i=i+1] = makeDynString ("","","","","adc1");
          msdpt[i=i+1] = makeDynString ("","","","","adc2");
          msdpt[i=i+1] = makeDynString ("","","","","adc3");
          msdpt[i=i+1] = makeDynString ("","","","","adc4");
          msdpt[i=i+1] = makeDynString ("","","","","adc5");
          msdpt[i=i+1] = makeDynString ("","","","","adc6");
          msdpt[i=i+1] = makeDynString ("","","","","adc7");
          msdpt[i=i+1] = makeDynString ("","","","","adc8");
          msdpt[i=i+1] = makeDynString ("","","","","adc9");
          msdpt[i=i+1] = makeDynString ("","","","","adc10");
          msdpt[i=i+1] = makeDynString ("","","","","adc11");
          msdpt[i=i+1] = makeDynString ("","","","","adc12");
          msdpt[i=i+1] = makeDynString ("","","","","adc13");
          msdpt[i=i+1] = makeDynString ("","","","","adc14");
          msdpt[i=i+1] = makeDynString ("","","","","adc15");

          msdpt[i=i+1] = makeDynString ("","","","","conf0");
          msdpt[i=i+1] = makeDynString ("","","","","conf1");
          msdpt[i=i+1] = makeDynString ("","","","","conf2");
          msdpt[i=i+1] = makeDynString ("","","","","conf3");
          msdpt[i=i+1] = makeDynString ("","","","","conf4");
          msdpt[i=i+1] = makeDynString ("","","","","conf5");
          msdpt[i=i+1] = makeDynString ("","","","","conf6");
          msdpt[i=i+1] = makeDynString ("","","","","conf7");
          msdpt[i=i+1] = makeDynString ("","","","","conf8");
          msdpt[i=i+1] = makeDynString ("","","","","conf9");
          msdpt[i=i+1] = makeDynString ("","","","","conf10");
          msdpt[i=i+1] = makeDynString ("","","","","conf11");
          msdpt[i=i+1] = makeDynString ("","","","","conf12");
          msdpt[i=i+1] = makeDynString ("","","","","conf13");
          msdpt[i=i+1] = makeDynString ("","","","","conf14");
          msdpt[i=i+1] = makeDynString ("","","","","conf15");

        msdpt[i=i+1] = makeDynString ("","","","sensor0","");

          msdpt[i=i+1] = makeDynString ("","","","","label");
          msdpt[i=i+1] = makeDynString ("","","","","name");
          msdpt[i=i+1] = makeDynString ("","","","","id");
          msdpt[i=i+1] = makeDynString ("","","","","idhigh");
          msdpt[i=i+1] = makeDynString ("","","","","idlow");
          msdpt[i=i+1] = makeDynString ("","","","","h1");
          msdpt[i=i+1] = makeDynString ("","","","","h2");
          msdpt[i=i+1] = makeDynString ("","","","","h3");
          msdpt[i=i+1] = makeDynString ("","","","","temp");
          msdpt[i=i+1] = makeDynString ("","","","","Bx");
          msdpt[i=i+1] = makeDynString ("","","","","By");
          msdpt[i=i+1] = makeDynString ("","","","","Bz");
          msdpt[i=i+1] = makeDynString ("","","","","Bcal");
          msdpt[i=i+1] = makeDynString ("","","","","state");
          msdpt[i=i+1] = makeDynString ("","","","","status");

        msdpt[i=i+1] = makeDynString ("","","","sensor1","");

          msdpt[i=i+1] = makeDynString ("","","","","label");
          msdpt[i=i+1] = makeDynString ("","","","","name");
          msdpt[i=i+1] = makeDynString ("","","","","id");
          msdpt[i=i+1] = makeDynString ("","","","","idhigh");
          msdpt[i=i+1] = makeDynString ("","","","","idlow");
          msdpt[i=i+1] = makeDynString ("","","","","h1");
          msdpt[i=i+1] = makeDynString ("","","","","h2");
          msdpt[i=i+1] = makeDynString ("","","","","h3");
          msdpt[i=i+1] = makeDynString ("","","","","temp");
          msdpt[i=i+1] = makeDynString ("","","","","Bx");
          msdpt[i=i+1] = makeDynString ("","","","","By");
          msdpt[i=i+1] = makeDynString ("","","","","Bz");
          msdpt[i=i+1] = makeDynString ("","","","","Bcal");
          msdpt[i=i+1] = makeDynString ("","","","","state");
          msdpt[i=i+1] = makeDynString ("","","","","status");

        msdpt[i=i+1] = makeDynString ("","","","sensor2","");

          msdpt[i=i+1] = makeDynString ("","","","","label");
          msdpt[i=i+1] = makeDynString ("","","","","name");
          msdpt[i=i+1] = makeDynString ("","","","","id");
          msdpt[i=i+1] = makeDynString ("","","","","idhigh");
          msdpt[i=i+1] = makeDynString ("","","","","idlow");
          msdpt[i=i+1] = makeDynString ("","","","","h1");
          msdpt[i=i+1] = makeDynString ("","","","","h2");
          msdpt[i=i+1] = makeDynString ("","","","","h3");
          msdpt[i=i+1] = makeDynString ("","","","","temp");
          msdpt[i=i+1] = makeDynString ("","","","","Bx");
          msdpt[i=i+1] = makeDynString ("","","","","By");
          msdpt[i=i+1] = makeDynString ("","","","","Bz");
          msdpt[i=i+1] = makeDynString ("","","","","Bcal");
          msdpt[i=i+1] = makeDynString ("","","","","state");
          msdpt[i=i+1] = makeDynString ("","","","","status");

        msdpt[i=i+1] = makeDynString ("","","","sensor3","");

          msdpt[i=i+1] = makeDynString ("","","","","label");
          msdpt[i=i+1] = makeDynString ("","","","","name");
          msdpt[i=i+1] = makeDynString ("","","","","id");
          msdpt[i=i+1] = makeDynString ("","","","","idhigh");
          msdpt[i=i+1] = makeDynString ("","","","","idlow");
          msdpt[i=i+1] = makeDynString ("","","","","h1");
          msdpt[i=i+1] = makeDynString ("","","","","h2");
          msdpt[i=i+1] = makeDynString ("","","","","h3");
          msdpt[i=i+1] = makeDynString ("","","","","temp");
          msdpt[i=i+1] = makeDynString ("","","","","Bx");
          msdpt[i=i+1] = makeDynString ("","","","","By");
          msdpt[i=i+1] = makeDynString ("","","","","Bz");
          msdpt[i=i+1] = makeDynString ("","","","","Bcal");
          msdpt[i=i+1] = makeDynString ("","","","","state");
          msdpt[i=i+1] = makeDynString ("","","","","status");

  //****************************************************//

  tmsdpt[j] = makeDynInt (DPEL_STRUCT);

    tmsdpt[j=j+1] = makeDynInt (0,DPEL_STRING);
    tmsdpt[j=j+1] = makeDynInt (0,DPEL_FLOAT);
    tmsdpt[j=j+1] = makeDynInt (0,DPEL_STRING);

    tmsdpt[j=j+1] = makeDynInt (0,DPEL_STRUCT); //node1

      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_INT);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_INT);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_INT);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRING);
      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_INT);

      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRUCT); //serial

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRING);

      tmsdpt[j=j+1] = makeDynInt (0,0,DPEL_STRUCT); //bf

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_UINT);
        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_INT);

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //data

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT); //adc
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_UINT);

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32); //conf
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_BIT32);

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //sensor0

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //label
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //name
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //id
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idh
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idl
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h1
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h2
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h3
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //temp
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bx
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //By
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bz
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bcal
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //state
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //status

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //sensor1

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //label
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //name
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //id
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idh
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idl
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h1
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h2
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h3
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //temp
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bx
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //By
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bz
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bcal
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //state
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //status

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //sensor2

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //label
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //name
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //id
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idh
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idl
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h1
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h2
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h3
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //temp
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bx
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //By
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bz
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bcal
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //state
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //status

        tmsdpt[j=j+1] = makeDynInt (0,0,0,DPEL_STRUCT); //sensor3

          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //label
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //name
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //id
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idh
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //idl
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h1
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h2
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_INT); //h3
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //temp
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bx
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //By
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bz
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_FLOAT); //Bcal
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //state
          tmsdpt[j=j+1] = makeDynInt (0,0,0,0,DPEL_STRING); //status

  //************************************************//

  n = dpTypeCreate(msdpt,tmsdpt);
}

void stgBmon_createDpsSector() {

  string prefix = "EIZ4";
  dyn_string sector = makeDynString("01", "03", "05", "07", "09", "11", "13", "15");

  for(int nSector = 1; nSector <= 8; nSector++) {
    dpCreate(prefix + "A" + sector[nSector], "NSWMDM_BField");
    dpCreate(prefix + "C" + sector[nSector], "NSWMDM_BField");
  }

  DebugN("Let's think.");
}

void stgBmon_createDpsInfrastructure() {

  dpCreate("bfieldPanel", "NSWMDM_BField_infrastructure");
  DebugN("Lets think");
}

void stgBmon_createDpsSimulation() {

  dyn_string sides = makeDynString("A", "C");

  for(int s = 1; s <= 2; s++) {
    for(int r = 1; r <= 2; r++) {
      for(int i = 1; i <= 16; i++) {

        dpCreate("EIZ" + r + stgBmon_sectorToSectorString(sides[s], i), "NSWMDMSimulation");
      }
    }
  }
}

void stgBmon_deleteDptInfrastructure() {

  dpTypeDelete("NSWMDM_BField_infrastructure");
}

void stgBmon_deleteDptSector() {

  dpTypeDelete("NSWMDM_BField");
}

void stgBmon_deleteDptSimulation() {

  dpTypeDelete("NSWMDMSimulation");
}

void stgBmon_deleteDpsInfrastructure() {

  dpDelete("bfieldPanel");
}

void stgBmon_deleteDpsSector() {

  string prefix = "EIZ4";

  dyn_string sector = makeDynString("01", "03", "05", "07", "09", "11", "13", "15");

  for(int nSector = 1; nSector <= 8; nSector++) {
    dpDelete(prefix + "A" + sector[nSector]);
    dpDelete(prefix + "C" + sector[nSector]);
  }
}

void stgBmon_deleteDpsSimulation() {

  dyn_string sides = makeDynString("A", "C");

  for(int s = 1; s <= 2; s++) {
    for(int r = 1; r <= 2; r++) {
      for(int i = 1; i <= 16; i++) {

        dpDelete("EIZ" + r + stgBmon_sectorToSectorString(sides[s], i));
      }
    }
  }
}

void stgBmon_archiveData() {

  string sys = stgBmon_returnProjectName();
  dyn_string exceptionInfo;
  string archiveClassName = "RDB-99) EVENT";
  int archiveType = DPATTR_ARCH_PROC_SIMPLESM;
  int smoothProcedure = DPATTR_TIME_AND_VALUE_REL_SMOOTH;
  float deadband = 5;
  float timeInterval = 3600;
  // float constantsTimeInterval = // Calculate this to be every 2-3 days
  dyn_string adcValues = dpNames(sys + "EIZ4*.node*.bf.data.adc*", "NSWMDM_BField");
  dyn_string sensorValues = dpNames(sys + "EIZ4*.node*.bf.sensor*.B*", "NSWMDM_BField");
  dyn_string idValues = dpNames(sys + "EIZ4*.node*.id", "NSWMDM_BField");
  dyn_string maskValues = dpNames(sys + "EIZ4*.node*.bf.bfMask", "NSWMDM_BField");

  for(int i = 1; i <= dynlen(adcValues); i++) {

    fwArchive_config(adcValues[i], archiveClassName, archiveType, smoothProcedure,
                         deadband, timeInterval, exceptionInfo);
    fwArchive_start(adcValues[i], exceptionInfo);
  }
  for(int j = 1; j <= dynlen(sensorValues); j++) {

    fwArchive_config(sensorValues[i], archiveClassName, archiveType, smoothProcedure,
                         deadband, timeInterval, exceptionInfo);
    fwArchive_start(sensorValues[i], exceptionInfo);
  }
  /*
  for(int k = 1; k <= dynlen(idValues); k++) {

    fwArchive_config(idValues[k], archiveClassName, archiveType, smoothProcedure,
                         deadband, constantsTimeInterval, exceptionInfo);
    fwArchive_start(idValues[k], exceptionInfo);
  }
  for(int m = 1; m <= dynlen(maskValues); m++) {

    fwArchive_config(maskValues[m], archiveClassName, archiveType, smoothProcedure,
                         deadband, constantsTimeInterval, exceptionInfo);
    fwArchive_start(maskValues[m], exceptionInfo);
  }*/
}
