//--------------------------------------------------------------------------------
/*
  @author Stamatios Tzanos, NTUA (stzanos: stamatios.tzanos@cern.ch)
*/
#uses "stgBmon/stgBmon_conversions.ctl"
#uses "stgBmon/stgBmon_constants.ctl"

string localSys = getSystemName();
string script = "STG_bfAvgCalculator.ctl: ";

main() {

  int state, N;
  float sum, Bcal;
  string state_datapoint, float_datapoint;

  information(script + "Started.");

  // Initialize datapoints
  information(script + "Initializing datapoints to calculate average values.");

  dyn_string chambers = dpNames("EIZ4*");
  if(dynlen(chambers) < 1) {
    error(script + "Datapoints do not exist.");
    return;
  }

  while(TRUE) {
    delay(5);
    for(int i = 1; i <= dynlen(chambers); i++) {
      N = 0;
      sum = 0;
      for(int n = 1; n <= 4; n++) {
        for(int sen = 0; sen < 4; sen++) {
          state_datapoint = chambers[i] + ".node" + n + ".bf.sensor" + sen + ".state";
          float_datapoint = chambers[i] + ".node" + n + ".bf.sensor" + sen + ".Bcal";

          if(dpExists(state_datapoint)) {
            dpGet(state_datapoint, state);
            if(state != BSENSOR_MASK && state != BSENSOR_DEAD && BSENSOR_BAD) {
              dpGet(float_datapoint, Bcal);
              N++;
              sum += Bcal;
            }
          }
          else
            error("Datapoint " + datapoint + " does not exist.");
        }
      }
      dpSetWait(chambers[i] + ".BcalAverage", sum/N);
    }
  }
}
