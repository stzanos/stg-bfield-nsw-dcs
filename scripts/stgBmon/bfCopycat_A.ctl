//--------------------------------------------------------------------------------
/*
  @author Stamatios Tzanos, NTUA (stzanos: stamatios.tzanos@cern.ch)
*/
#uses "stgBmon/stgBmon_conversions.ctl"
#uses "stgBmon/stgBmon_constants.ctl"

string localSys = getSystemName();
string remoteSys = ""; // "ATLMDTMDM4:";
string side = "A";
string script = "STG_bfCopycat_" + side + ".ctl: ";

main() {

  bool connected = FALSE;
  int count = 0;

  information(script + "Started.");

  /*
  // Checking for connection to remote system
  while (!connected) {
    unDistributedControl_isConnected(connected, remoteSys);
    if (!connected) {
      information(script + "No connection to remote project. Waiting ....");
      delay(30.); count++;
    }
    if (count > 10) {
      error(script + "Remote system " + remoteSys + "could not be reached. Aborting.");
      return;
    }
  }

  information("Delay 20 sec for reliable connection to remote system");
  delay(20);
  information("End of delay");
  */

  // Initialize datapoints
  information(script + "Initializing copy mechanism.");

  dyn_string remoteChambers = dpNames(remoteSys + "EIZ*" + side + "*", "NSWMDMSimulation");
  if(dynlen(remoteChambers) < 1) {
    error(script + "Remote datapoints do not exist.");
    return;
  }

  dyn_string localChambers = dpNames("EIZ4" + side + "*", /* stgBmon_returnProjectName() + */"NSWMDM_BField");
  if(dynlen(localChambers) < 1) {
    error(script + "local datapoints do not exist.");
    return;
  }

  for(int i = 1; i <= dynlen(remoteChambers); i++) {

    dpConnect_nodeInfo(remoteChambers[i] + ".node.");
    dpConnect_bfInfo(remoteChambers[i] + ".node.bf.");
    dpConnect_bfData(remoteChambers[i] + ".node.bf.data.adc");
    dpConnect_sensorData(remoteChambers[i] + ".node.bf.sensor");
  }

  information(script + "Finished.");
}

void copyValue(string remote_dpe, anytype remote_value) {

  anytype local_value;
  string local_dpe = remoteToLocal(remote_dpe);

  dpGet(local_dpe, local_value);

  if(local_value != remote_value)
    dpSetWait(local_dpe, remote_value);
}

void dpConnect_nodeInfo(string dp) {

  dyn_string dpes = makeDynString("canbus", "canport", "serial.serialnr",
                                  "id", "busid", "state");

  for(int i = 1; i <= dynlen(dpes); i++) {
    if(dpConnect("copyValue", dp + dpes[i]) != 0)
      error(script + "dpConnect failed for datapoint " + dp + dpes[i]);
  }
}

void dpConnect_bfInfo(string dp) {

  dyn_string dpes = makeDynString("bfMask", "state");

  for(int i = 1; i <= dynlen(dpes); i++) {
    if(dpConnect("copyValue", dp + dpes[i]) != 0)
      error(script + "dpConnect failed for datapoint " + dp + dpes[i]);
  }
}

void dpConnect_bfData(string dp) {

  for(int i = 0; i < 16; i++) {
    if(dpConnect("copyValue", dp + i) != 0)
      error(script + "dpConnect failed for datapoint " + dp + i);
  }

}

void dpConnect_sensorData(string dp) {

  dyn_string dpes = makeDynString("state", "idhigh", "idlow", "id",
                                  "h1", "h2", "h3", "temp",
                                  "Bx", "By", "Bz", "Bcal");

  for(int j = 0; j < 4; j++) {
    for(int i = 1; i <= dynlen(dpes); i++) {
      if(dpConnect("copyValue", dp + j + "." + dpes[i]) != 0)
        error(script + "dpConnect failed for datapoint " + dp + j + "." + dpes[i]);
    }
  }
}

string remoteToLocal(string remote_dpe) {

  string local_dpe;
  int node;
  string largeSector;

  string temp_dpe = dpSubStr(remote_dpe, DPSUB_DP);
  int radius = substr(temp_dpe, 3, 1);
  string sector = substr(temp_dpe, 5, 2);

  if(radius == 1) {
    switch(sector) {
      case "01":
        node = 3;
        largeSector = "01";
        break;
      case "02":
        node = 1;
        largeSector = "03";
        break;
      case "03":
        node = 3;
        largeSector = "03";
        break;
      case "04":
        node = 1;
        largeSector = "05";
        break;
      case "05":
        node = 3;
        largeSector = "05";
        break;
      case "06":
        node = 1;
        largeSector = "07";
        break;
      case "07":
        node = 3;
        largeSector = "07";
        break;
      case "08":
        node = 1;
        largeSector = "09";
        break;
      case "09":
        node = 3;
        largeSector = "09";
        break;
      case "10":
        node = 1;
        largeSector = "11";
        break;
      case "11":
        node = 3;
        largeSector = "11";
        break;
      case "12":
        node = 1;
        largeSector = "13";
        break;
      case "13":
        node = 3;
        largeSector = "13";
        break;
      case "14":
        node = 2;
        largeSector = "15";
        break;
      case "15":
        node = 3;
        largeSector = "15";
        break;
      case "16":
        node = 2;
        largeSector = "01";
        break;
    }
  }
  else {
    switch(sector) {
      case "01":
        node = 4;
        largeSector = "01";
        break;
      case "02":
        node = 2;
        largeSector = "03";
        break;
      case "03":
        node = 4;
        largeSector = "03";
        break;
      case "04":
        node = 2;
        largeSector = "05";
        break;
      case "05":
        node = 4;
        largeSector = "05";
        break;
      case "06":
        node = 2;
        largeSector = "07";
        break;
      case "07":
        node = 4;
        largeSector = "07";
        break;
      case "08":
        node = 2;
        largeSector = "09";
        break;
      case "09":
        node = 4;
        largeSector = "09";
        break;
      case "10":
        node = 2;
        largeSector = "11";
        break;
      case "11":
        node = 4;
        largeSector = "11";
        break;
      case "12":
        node = 2;
        largeSector = "13";
        break;
      case "13":
        node = 4;
        largeSector = "13";
        break;
      case "14":
        node = 1;
        largeSector = "15";
        break;
      case "15":
        node = 4;
        largeSector = "15";
        break;
      case "16":
        node = 1;
        largeSector = "01";
        break;
    }
  }

  local_dpe = dpSubStr(remote_dpe, DPSUB_DP_EL);
  strreplace(local_dpe, "EIZ" + radius + side + sector, "EIZ4" + side + largeSector);
  strreplace(local_dpe, "node", "node" + node);

  return local_dpe;
}
