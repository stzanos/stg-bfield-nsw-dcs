//--------------------------------------------------------------------------------
/*
  @author Stamatios Tzanos, NTUA (stzanos: stamatios.tzanos@cern.ch)
*/

#uses "stgBmon/stgBmon_conversions.ctl"
#uses "stgBmon/stgBmon_constants.ctl"

main() {

  DebugN("Simulator has started.");

  string sys = getSystemName();
  dyn_string chambers_A = dpNames("EIZ*A*", "NSWMDMSimulation");
  dyn_string chambers_C = dpNames("EIZ*C*", "NSWMDMSimulation");

  // whether last two sensors are masked or not
  dyn_bool masked = makeDynInt(1, 1, 1, 1, //STG
                               1, 1, 1, 1,
                               1, 1, 1, 1,
                               1, 0, 1, 0,
                               0, 0, 0, 0, //MMG
                               0, 0, 0, 0,
                               0, 0, 0, 0,
                               0, 1, 0, 1);

  string chamber_type;
  string sector;
  int n_max, mask;
  dyn_uint adc_A, adc_C;
  dyn_int hdata_A, hdata_C;
  dyn_float bdata_A, bdata_C;
  int n = 0;

  while(1) {


    for(int i = 1; i <= dynlen(chambers_A); i++) {

      if(masked[i]) {
        n_max = 8;
        mask = 3;
      }
      else {
        n_max = 16;
        mask = 16;
      }

      for(int n = 1; n <= 16; n++) {

        if(n % 4  == 0 && n <= n_max) {
          adc_A[n] = 25000 + secureRandom()/33;
          adc_C[n] = 25000 - secureRandom()/33;
        }
        else if(n <= n_max) {
          adc_A[n] = secureRandom();
          adc_C[n] = secureRandom();
        }
        else {
          adc_A[n] = 0;
          adc_C[n] = 0;
        }

        dpSetWait(chambers_A[i] + ".node.bf.data.adc" + (n-1), adc_A[n]);
        dpSetWait(chambers_C[i] + ".node.bf.data.adc" + (n-1), adc_C[n]);
      }

      hdata_A = setHdata(adc_A, chambers_A[i]);
      hdata_C = setHdata(adc_C, chambers_C[i]);
      bdata_A = setBdata(hdata_A, chambers_A[i]);
      bdata_C = setBdata(hdata_C, chambers_C[i]);
      setBcal(bdata_A, chambers_A[i]);
      setBcal(bdata_C, chambers_C[i]);

      adc_A = makeDynUInt(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
      adc_C = makeDynUInt(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    }

    delay(2);
    if(n % 10 == 0) DebugN("Simulation loop " + n + ", done.");
    n++;
  }
}




// ------------------------
// Calculate H1, H2, H3 values for every sensor of a sector from
// the adc fields being read out by the MDM.
// ------------------------
dyn_int setHdata(dyn_uint data, string dp) {

  dyn_int hdata;
  dyn_string h = makeDynString(".h1", ".h2", ".h3");
  string dp_new;

  for(int i = 1; i <= dynlen(data); i++) {

    if(i % 4 == 0) {

      hdata[i] = data[i]; // This is the temperature in mdeg
      dp_new = dp + ".node.bf.sensor" + stgBmon_adcFieldToSensorInput(i-1) + ".temp";
      dpSetWait(dp_new, hdata[i]/1000);
      if(hdata[i] > 100000)
        dpSetWait(dp + ".node.bf.sensor" + stgBmon_adcFieldToSensorInput(i-1) + ".state", BFSENSOR_TEMP);
    }
    else {

      hdata[i] = stgBmon_convertUIntSimulation(data[i]);
      dp_new = dp + ".node.bf.sensor" + stgBmon_adcFieldToSensorInput(i-1) + h[i%4];
      dpSetWait(dp_new, hdata[i]);
    }
  }

  return hdata;
}

// ------------------------
// Calculate Bx, By, Bz values for every sensor of a sector from
// H1, H2, H3 values.
// ------------------------
dyn_float setBdata(dyn_int hdata, string dp) {

  dyn_float bdata;
  dyn_string b = makeDynString(".Bx", ".By", ".Bz");
  string dp_new;

  for(int i = 1; i <= dynlen(hdata); i++) {

    if(i % 4 == 0) bdata[i] = 0; // This is the temperature field
    else {

      if(hdata[i] == 0) bdata[i] = 0;
      else bdata[i] = stgBmon_convertHallValue(hdata[i]);

      dp_new = dp + ".node.bf.sensor" + stgBmon_adcFieldToSensorInput(i-1) + b[i%4];
      dpSetWait(dp_new, bdata[i]);
    }
  }

  return bdata;
}

// ------------------------
// Calculate the B field values of a sector from Bx, By, Bz of
// every sensor.
// ------------------------
void setBcal(dyn_float bdata, string dp) {

  for(int i = 0; i < 4; i++) {

    dpSetWait(dp + ".node.bf.sensor" + i + ".Bcal", vector_3D_abs(bdata[i*4+1], bdata[i*4+2], bdata[i*4+3]));
  }
}


float vector_3D_abs(float x, float y, float z) {

  return sqrt(x*x + y*y + z*z);
}
